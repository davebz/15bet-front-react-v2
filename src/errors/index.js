import React, { Component } from "react";
class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    // You can also log the error to an error reporting service
    //logErrorToMyService(error, info);
    console.log(error, info);
  }

  //Invoke this in child component to show fallback UI
  showError(){
    console.log('showing generic error');
    let _this=this;
    _this.setState({ hasError: true });
    setTimeout(function(){ _this.setState({hasError:false}) }, 1500);
  }

  render() {
    const childrenWithProps = [this.props.children].map((children, key) =>
        React.cloneElement(children, { key, showError: this.showError.bind(this) })
        );

    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
          <React.Fragment>
            <div className="col-md-10 account_content">
              <div
                className="tab-pane fade active show"
                id="withdrawal"
                role="tabpanel"
                aria-labelledby="withdrawal-tab"
              >
                <section class="error-container text-center">
                  <h1>500</h1>
                  <div class="error-divider">
                    <h2>ooops!!</h2>
                    <p class="description">SOMETHING WENT WRONG.</p>
                  </div>
                </section>
              </div>
            </div>
          </React.Fragment>)
    }else{

      return <React.Fragment>{childrenWithProps}</React.Fragment>
    }
  }
}

export default ErrorBoundary;
