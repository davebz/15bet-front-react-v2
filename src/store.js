import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers";
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';

const inititalState = {};
const env = process.env.NODE_ENV;
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReducer,
  inititalState,
  compose(
    applyMiddleware(sagaMiddleware),
    env === "development"
      ? window.__REDUX_DEVTOOLS_EXTENSION__ &&
          window.__REDUX_DEVTOOLS_EXTENSION__()
      : ""
  )
);

sagaMiddleware.run(rootSaga);

export default store;
