import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { postDeposit, checkDeposit } from "../../../actions/payment";
import classnames from "classnames";
import Wallet from "../../layouts/Wallet";

class Deposit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: "0.00",
            sender: "",
            provider: "",
            success: false,
            error: false,
            validated: false,
        };
    }

    componentDidMount() {
        this.props.checkDeposit();
    }

    componentWillReceiveProps(nextProps) {
        try {
            if (nextProps.errors.provider) {
                this.setState({ error: true });
                this.setState({ success: false });
            }

            if (nextProps.errors.sender) {
                this.setState({ error: true });
                this.setState({ success: false });
            }

            if (nextProps.errors.amount) {
                this.setState({ error: true });
                this.setState({ success: false });
            }

            if (nextProps.success.length > 0) {
                this.setState({ error: false });
                this.setState({ success: true });
            }
        } catch (error) {
            nextProps.showError();
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    resetForm = () => {
        this.setState({ amount: 0 });
    };

    onSubmit = (e) => {
        e.preventDefault();

        const payment = {
            amount: Number(this.state.amount.replace(/[^0-9-.]/g, "")),
            sender: this.state.sender,
            provider: this.state.provider
        };

        this.setState({ validated: true });
        if(this.form.checkValidity()){
            this.props.postDeposit(payment);
            this.setState({ validated: false });
        }

    };

    quickAddAmount = (amountToAdd) => {
        let currentAmount = this.state.amount;
        if (currentAmount) {
            currentAmount = Number(currentAmount.replace(/[^0-9-.]/g, ""));
        }
        if (String(currentAmount).match(/[a-z]/i)) {
            currentAmount = 0;
        }
        currentAmount = parseFloat(currentAmount) + parseFloat(amountToAdd);
        currentAmount = currentAmount
            .toFixed(2)
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        this.setState({ amount: currentAmount });
    };

    refreshPage = () => {
        window.location.reload();
    };

    render() {
        const pendingDeposit = this.props.deposit;
        const errors = this.props.errors;

        return (
            
            <form
            className={this.state.validated ? "needs-validation was-validated": ""}
                noValidate method="post"
                onSubmit={this.onSubmit.bind(this)}
                ref={(form)=> this.form = form}>

                <div className="formContent">
                    <div className="formContentWrap pl-5 pr-5">
                        <div className="row">
                            {this.state.success || pendingDeposit > 0 ? (
                                <div className="col-md-7" id="deposit-message">
                                    <h2 className="formTitle">
                                        Deposit Request Sent!
                                    </h2>
                                    <p>
                                        Your request is submitted and
                                        waiting for review.
                                    </p>
                                    <p>
                                        We sent our bank details to you're
                                        registered mobile number
                                    </p>
                                    <p>
                                        Pls., deposit the amount submitted
                                        in no longer than 2 banking days
                                    </p>
                                </div>
                            ) : (
                                <div className="col-md-7" id="deposit-form">
                                    <h2 className="formTitle">Deposit</h2>

                                    <div className="form-group row">
                                        <label htmlFor="method" className="col-sm-4 col-form-label">
                                            Payment Method
                                        </label>
                                        <div className="col-sm-8">
                                            <select className="custom-select" id="method" disabled>
                                                <option value="Local Transfer" defaultValue>
                                                    Local Transfer
                                                </option>
                                            </select>
                                            <small className="form-text text-muted" >
                                                (Min: 10000.00 / Max: 100000.00)
                                            </small>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label htmlFor="provider" className="col-sm-4 col-form-label">
                                            Wallet Option <span className="red">*</span>
                                        </label>

                                        <div className="col-sm-8">
                                            <div className="input-group-prepend">
                                                <select className={classnames("custom-select", { "is-invalid": errors && errors.provider })} id="provider" name="provider" onChange={this.onChange} required>
                                                    <option value="">
                                                        Select Wallet
                                                    </option>
                                                    <option value="1">
                                                        Oneworks
                                                    </option>
                                                </select>
                                                <div className={classnames("invalid-tooltip", { "d-block": errors && errors.provider })} >
                                                    {errors && errors.provider ? errors.provider : 'The provider field is required.'}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label htmlFor="currency" className="col-sm-4 col-form-label" >
                                            Currency
                                        </label>
                                        <div className="col-sm-8">
                                            <select className="custom-select" id="currency" disabled>
                                                <option defaultValue>
                                                    {this.props.currency}
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label htmlFor="sender" className="col-sm-4 col-form-label">
                                            Sender Name <span className="red">*</span>
                                        </label>
                                        <div className="col-sm-8">
                                            <input type="text" className={classnames("form-control", { "is-invalid": errors && errors.sender })} id="sender" name="sender" placeholder="Zilong Legend" onChange={this.onChange} required />
                                            <div className={classnames("invalid-tooltip", { "d-block": errors && errors.sender })} >
                                                {errors && errors.sender ? errors.sender : 'The sender field is required.'}
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <div className="btn-quick-amount text-right">
                                                <button className="btn-default btn-add-amount" type="button"  
                                                onClick={this.quickAddAmount.bind(this, 30000 )}
                                                > +30K
                                                </button>
                                                <button className="btn-default btn-add-amount" type="button"
                                                    onClick={this.quickAddAmount.bind(this, 100000 )}
                                                > +100K
                                                </button>
                                                <button className="btn-default btn-add-amount" type="button"
                                                    onClick={this.quickAddAmount.bind(this, 500000)}
                                                >+500K
                                                </button>
                                                <button className="btn-default btn-add-amount" type="button"
                                                    onClick={this.quickAddAmount.bind(this,1000000)}
                                                > +1M
                                                </button>
                                                <div className="clear-fix" />
                                            </div>
                                        </div>

                                        <div className="col-md-12">
                                            <div className="form-group row text-left">
                                                <label htmlFor="amount_amount" className="col-sm-4 col-form-label">
                                                    Deposit Amount{" "}<span className="red">* </span>
                                                </label>
                                                <div className="col-sm-8">
                                                    <input type="text" id="amount" name="amount" className="form-control" placeholder="100,000"
                                                        onChange={ this.onChange }
                                                        value={ this.state.amount}
                                                        className={classnames("form-control", { "is-invalid": errors && errors.amount })}
                                                        required
                                                    />
                                                    <small className="form-text text-muted">
                                                        (Min: 50,000.00)
                                                    </small>
                                                    <div className={classnames("invalid-tooltip", { "d-block": errors && errors.amount })} >
                                                        {errors && errors.amount ? errors.amount : 'The withdrawal amount field is required.'}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group form-check text-center">
                                        <input
                                            type="checkbox"
                                            className="form-check-input"
                                            id="exampleCheck1"
                                            required
                                        />
                                        <label
                                            className="form-check-label"
                                            htmlFor="exampleCheck1"
                                        >
                                            I agree to the{" "}
                                            <a href="#tnc">
                                                Terms &amp; Conditon
                                            </a>{" "}
                                            of 15bet{" "}
                                            <span className="red">*</span>
                                        </label>
                                        <p style={{ fontSize: 12 }}>
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipisicing elit.{" "}
                                            <br />
                                            Odio magnam esse fuga quam,
                                            explicabo, quod voluptatem
                                            laboriosam similique!
                                            <br /> Facilis eos natus, vero.
                                            Quasi, magni eaque.
                                            <br /> Consequuntur aut error
                                            ipsam quasi s, <br />
                                            vero. Quasi, magni eaque.
                                            Consequuntur aut error ipsam
                                            quasi.
                                        </p>
                                    </div>

                                    <div className="action-btn-modal text-center">
                                        <button
                                            onClick={this.resetForm}
                                            className="btn btn-default"
                                            type="reset"
                                        >
                                            RESET
                                        </button>{" "}
                                        <button
                                            className="btn btn-success"
                                            type="submit"
                                        >
                                            SUBMIT
                                        </button>
                                    </div>
                                </div>
                            )}

                            <Wallet />
                        </div>
                    </div>
                </div>
            </form>
        
        );
    }
}

Deposit.propTypes = {
    postDeposit: PropTypes.func.isRequired,
    checkDeposit: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    errors: state.payment.depositErrors,
    success: state.payment.depositResult,
    deposit: state.payment.transaction.transaction,
    currency: state.payment.transaction.currency,
    pendingDeposit: state.payment.pendingDeposit
});

export default connect(
    mapStateToProps,
    { postDeposit, checkDeposit }
)(withRouter(Deposit));
