import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

import {
  selectDate,
  selectType,
  getTransactionHistory
} from "../../../actions/transaction";

class TransactionHistory extends Component {
  componentDidMount() {
    this.props.getTransactionHistory();
  }

  componentWillReceiveProps(nextProps) {}

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    let { transactionHistory } = this.props.transactionHistory;

    return (
        
        <div class="formContent">
            <div class="formContentWrap pl-5 pr-5">
                <div
                    className="tab-pane fade active show"
                    id="withdrawal"
                    role="tabpanel"
                    aria-labelledby="withdrawal-tab"
                >
                    <form onSubmit={this.onSubmit}>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="row">
                                    <div className="col-md-12">
                                        <h2>Transaction History</h2>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="form-group filter-history no-gutters">
                                            <div className="row">
                                                <div className="col-sm-4 form-inline max180">
                                                   <p>Filter by date:</p>
                                                   
                                                        <select
                                                            className="custom-select"
                                                            onChange={this.props.selectDate}
                                                        >
                                                            <option value={0} defaultValue>
                                                                Today
                                                            </option>
                                                            <option value={1}>Yesterday</option>
                                                            <option value={3}>Last 3 Days</option>
                                                            <option value={7}>Last 7 Days</option>
                                                        </select>
                                                    </div>

                                                    <div className="col-sm-4 form-inline float-right">
                                                    <p> Filter by Transaction Type: </p>
                                                        <select
                                                            className="custom-select"
                                                            onChange={this.props.selectType}
                                                        >
                                                            <option value={0} defaultValue>
                                                                All
                                                            </option>
                                                            <option value={1000}>Deposit</option>
                                                            <option value={2000}>Withdrawal</option>
                                                            <option value={3000}>Transfer</option>
                                                        </select>
                                                    </div>

                                                    <div className="col-sm-2 form-inline max180">
                                                    <p> From:</p>
                                                        <select className="custom-select">
                                                            <option value={1} defaultValue>
                                                                Main Wallet
                                                            </option>
                                                            <option value={2}>KENO Gaming</option>
                                                            <option value={3}>LE Gaming</option>
                                                            <option value={4}>SG Win</option>
                                                        </select>
                                                    </div>
                                                    <div className="col-sm-2 form-inline max180">
                                                        <p> To:</p>
                                                        <select className="custom-select">
                                                            <option value={1}>Main Wallet</option>
                                                            <option value={2} defaultValue>
                                                                KENO Gaming
                                                            </option>
                                                            <option value={3}>LE Gaming</option>
                                                            <option value={4}>SG Win</option>
                                                        </select>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <table className="table transaction-history">
                                                <thead className="thead-dark">
                                                    <tr>
                                                        <th style={{ width: "3%" }}>#</th>
                                                        <th style={{ width: "17%" }}>Date</th>
                                                        <th style={{ width: "17%" }}>Transaction Type</th>
                                                        <th style={{ width: "12%" }}>From</th>
                                                        <th style={{ width: "12%" }}>To</th>
                                                        <th style={{ width: "16%" }}>Reference #</th>
                                                        <th style={{ width: "10%", textAlign: "center" }}>Amount</th>
                                                        <th style={{ width: "13%", textAlign: "center" }}>Status</th>

                                                    </tr>
                                                </thead>
                                                {transactionHistory.length > 0 ? (
                                                    <tbody>
                                                        {transactionHistory.map((history, id) => {
                                                            return (
                                                                <tr key={id}>
                                                                    <td>{id + 1}</td>
                                                                    <td>{history.Date}</td>
                                                                    <td>{history.TransactionType}</td>
                                                                    <td>{history.From}</td>
                                                                    <td>{history.To}</td>
                                                                    <td>{history.ReferenceNo}</td>
                                                                    <td className={"text-right"}>
                                                                        {history.Amount}
                                                                    </td>
                                                                    <td style={{ width: "15%", textAlign: "center" }}>
                                                                        <span style={{ color: history.Color }}>
                                                                            {history.Status}
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            );
                                                        })}
                                                    </tbody>
                                                ) : (
                                                    <tbody className="text-center">
                                                        <tr>
                                                            <td colSpan="12">
                                                                <h4>No Results Found</h4>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                )}
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
    );
  }
}

TransactionHistory.propTypes = {
  getTransactionHistory: PropTypes.func.isRequired,
  selectType: PropTypes.func,
  transactionHistory: PropTypes.object,
  transactionStatus: PropTypes.object,
  transactionStatusColor: PropTypes.object,
  transactionTypes: PropTypes.object
};

const mapStateToProps = state => ({
  errors: state.transactionHistory.errors,
  success: state.transactionHistory.result,
  ...state
});

export default connect(
  mapStateToProps,
  { getTransactionHistory, selectType, selectDate }
)(withRouter(TransactionHistory));
