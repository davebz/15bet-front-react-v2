import React, { Component } from "react";

class NotFound extends Component {
  render() {
    return (
      <div
        className="container-fluid"
        style={{ textAlign: "center", margin: "130px auto" }}
      >
        <h1 className="display-4">
          <span className="text-danger">404</span> Page Not Found
        </h1>
        <p className="lead" style={{ color: "#fff" }}>
          Sorry, that page does not exist
        </p>
      </div>
    );
  }
}

export default NotFound;
