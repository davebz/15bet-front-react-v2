import React, { Component } from "react";
import Wallet from "../../layouts/Wallet";

class Transfer extends Component {
  componentDidMount() {}

  componentWillReceiveProps(nextProps) {}

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  refreshPage = () => {
    window.location.reload();
  };

  render() {
    return (

      <form onSubmit={this.onSubmit}>
          <div className="formContent">
              <div className="formContentWrap pl-5 pr-5">
                  <div className="row">
                      <div className="col-md-7">
                          <div className="row">
                              <div className="col-md-12">
                                  <h2 className="formTitle">Transfer</h2>
                              </div>
                          </div>
                      </div>
                      <Wallet />
                </div>
            </div>
        </div>
      </form>
        
      
    );
  }
}

export default Transfer;
