import React, { Component } from "react";
import Slider from "react-slick";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getBanners } from "../../../actions/banner";
import { Link } from "react-router-dom";

class Feature extends Component {
    componentDidMount() {
        this.props.getBanners();
    }

    render() {
        const settings = {
          dots: true,
          arrows: false,
          autoplay: false
        };

        const { banners } = this.props;

        return (
            <section id="featured">
                <Slider {...settings} className="slider">
                    {banners.map(banner => (
                        <div class="slide-content" key={banner.BannerID}>
                            <div class="row">
                                <div class="col-md-12">
                                    <Link to={banner.Url} >
                                        <img  src={banner.Image} className="img-fluid" alt={banner.Title} />
                                    </Link>
                                </div>
                                <div class="col-md-5 offset-md-7 meta">
                                    <h2>{banner.Title}</h2>
                                    <h3>{banner.Subtitle}</h3>
                                    <Link to={banner.Url} className="btn btn-primary btn-lg view-game" >
                                       {banner.ButtonText}
                                    </Link>
                                </div>
                            </div>
                        </div>
                    ))}
                </Slider>
            </section>
        );
    }
}

Feature.propTypes = {
    banners: PropTypes.array.isRequired,
    getBanners: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    banners: state.banners.banners
});

export default connect(
    mapStateToProps,
    { getBanners }
)(Feature);
