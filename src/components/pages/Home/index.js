import React, { Component } from "react";
import { Link } from "react-router-dom";
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col} from "reactstrap";
import classnames from "classnames";
import Feature from "./Feature";

class Home extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: "1"
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {
        
        return (
            <div>
                <Feature />
                <section id="subScreen">
                    <div className="container">
                        <div className="row subCTA">
                            <div className="col-md-6">
                                <div className="card">
                                    <Link to="/bet" className="btn btn-success">
                                        BET NOW
                                    </Link>
                                    <Link to="/bet">
                                        <img
                                            src="assets/img/sub.jpg"
                                            className="img-fluid"
                                        />
                                    </Link>
                                    <h3>15 Sports | European View</h3>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="card">
                                    <Link to="/bet" className="btn btn-success">
                                        BET NOW
                                    </Link>
                                    <Link to="/bet">
                                        <img
                                            src="assets/img/sub-2.jpg"
                                            className="img-fluid"
                                        />
                                    </Link>
                                    <h3>OW Sports | Asian View</h3>
                                </div>
                            </div>
                        </div>

                        <div className="row subCashback">
                            <div className="col-md-12">
                                <a href="#">
                                    <img
                                        src="assets/img/home/cashback-banner.jpg"
                                        className="img-fluid cashbackBanner"
                                    />
                                </a>
                            </div>
                        </div>

                        <div className="row subLive">
                            <div className="col">
                                <div className="card">
                                    <h3>Live Calendar</h3>
                                    <img
                                        src="assets/img/Demo/live_1.jpg"
                                        className="img-fluid"
                                    />
                                </div>
                            </div>

                            <div className="col-md-5 text-center">
                                <div className="card">
                                    <h3>Live Highlights</h3>
                                    <img
                                        src="assets/img/Demo/live_2.jpg"
                                        className="img-fluid"
                                    />
                                </div>
                            </div>

                            <div className="col">
                                <div className="card">
                                    <h3>Live Centre</h3>
                                    <img
                                        src="assets/img/Demo/live_3.jpg"
                                        className="img-fluid"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="livecasinoHome">
                    <div className="title-livecasino text-center">
                        <a href=".casino.html">
                            <img src="assets/img/home/LIVEcasino.png" />
                        </a>
                    </div>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-3">
                                <a href="#">
                                    <div className="provider-1" />
                                </a>
                            </div>
                            <div className="col-md-3">
                                <a href="#">
                                    <div className="provider-2" />
                                </a>
                            </div>
                            <div className="col-md-3">
                                <a href="#">
                                    <div className="provider-3" />
                                </a>
                            </div>
                            <div className="col-md-3">
                                <a href="#">
                                    <div className="provider-4" />
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="gamesFeatured">
                    <div className="title-livecasino text-center">
                        <a href=".casino.html">
                            <img
                                src="assets/img/home/slot.png"
                                className="mb-5"
                            />
                        </a>
                    </div>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-7">
                                <Nav tabs id="featGameTab">
                                    <NavItem>
                                        <NavLink
                                            className={classnames({
                                                active:
                                                    this.state.activeTab === "1"
                                            })}
                                            onClick={() => {
                                                this.toggle("1");
                                            }}
                                        >
                                            Jackpot Games
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({
                                                active:
                                                    this.state.activeTab === "2"
                                            })}
                                            onClick={() => {
                                                this.toggle("2");
                                            }}
                                        >
                                            Popular Games
                                        </NavLink>
                                    </NavItem>
                                </Nav>

                                <TabContent activeTab={this.state.activeTab}>
                                    <TabPane tabId="1">
                                        <Row>
                                            <Col>
                                                <div className="casinoHome">
                                                    <div className="row casinoGameFeed no-gutters">
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/1.jpg"
                                                                    alt="g-1"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/2.jpg"
                                                                    alt="g-2"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/3.jpg"
                                                                    alt="g-3"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/4.jpg"
                                                                    alt="g-4"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/5.jpg"
                                                                    alt="g-5"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/6.jpg"
                                                                    alt="g-6"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/6.jpg"
                                                                    alt="g-6"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/12.jpg"
                                                                    alt="g-12"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/8.jpg"
                                                                    alt="g-8"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/9.jpg"
                                                                    alt="g-9"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/10.jpg"
                                                                    alt="g-10"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/11.jpg"
                                                                    alt="g-11"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </TabPane>
                                    <TabPane tabId="2">
                                        <Row>
                                            <Col>
                                                <div className="casinoHome">
                                                    <div className="row casinoGameFeed no-gutters">
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/12.jpg"
                                                                    alt="g-12"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/13.jpg"
                                                                    alt="g-13"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/14.jpg"
                                                                    alt="g-14"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/11.jpg"
                                                                    alt="g-11"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/15.jpg"
                                                                    alt="g-15"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/16.jpg"
                                                                    alt="g-16"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/17.jpg"
                                                                    alt="g-17"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/1.jpg"
                                                                    alt="g-1"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/2.jpg"
                                                                    alt="g-2"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/4.jpg"
                                                                    alt="g-4"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/5.jpg"
                                                                    alt="g-5"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-3">
                                                            <div className="card">
                                                                <img
                                                                    src="assets/img/games/6.jpg"
                                                                    alt="g-6"
                                                                />
                                                                <h3>
                                                                    Lucky
                                                                    Holloween
                                                                </h3>
                                                                <div className="ctaCard text-center">
                                                                    <a
                                                                        href="#"
                                                                        className="btn btn-success"
                                                                    >
                                                                        PLAY NOW
                                                                    </a>
                                                                    <a href="#">
                                                                        PLAY
                                                                        DEMO
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </TabPane>
                                </TabContent>
                            </div>
                            <div className="col-md-5">
                                <div className="card home-progressive">
                                    <div className="prog-title row">
                                        <img
                                            src="assets/img/home/progressive-jackpot.png"
                                            className="prog-image"
                                        />
                                    </div>
                                    <div className="container">
                                        <div className="row no-gutters progressive-games-first">
                                            <div className="progressive-column col-6">
                                                <div className="progressive-list ctaProg">
                                                    <img
                                                        src="assets/img/games/progressive/prog-1.png"
                                                        className="img-fluid progressive-thumbnails"
                                                    />
                                                    <p className="prog-game-name">
                                                        $999,999,999.00
                                                    </p>
                                                    <div className="ctaCardProg text-center">
                                                        <a
                                                            href="#"
                                                            className="btn btn-success"
                                                        >
                                                            PLAY NOW
                                                        </a>
                                                        <a href="#" className>
                                                            PLAY DEMO
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="progressive-column col-6">
                                                <div className="progressive-list ctaProg">
                                                    <img
                                                        src="assets/img/games/progressive/prog-2.png"
                                                        className="img-fluid progressive-thumbnails"
                                                    />
                                                    <p className="prog-game-name">
                                                        $999,999,999.00
                                                    </p>
                                                    <div className="ctaCardProg text-center">
                                                        <a
                                                            href="#"
                                                            className="btn btn-success"
                                                        >
                                                            PLAY NOW
                                                        </a>
                                                        <a href="#" className>
                                                            PLAY DEMO
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="progressive-column col-6">
                                                <div className="progressive-list ctaProg">
                                                    <img
                                                        src="assets/img/games/progressive/prog-3.png"
                                                        className="img-fluid progressive-thumbnails"
                                                    />
                                                    <p className="prog-game-name">
                                                        $999,999,999.00
                                                    </p>
                                                    <div className="ctaCardProg text-center">
                                                        <a
                                                            href="#"
                                                            className="btn btn-success"
                                                        >
                                                            PLAY NOW
                                                        </a>
                                                        <a href="#" className>
                                                            PLAY DEMO
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="progressive-column col-6">
                                                <div className="progressive-list ctaProg">
                                                    <img
                                                        src="assets/img/games/progressive/prog-4.png"
                                                        className="img-fluid progressive-thumbnails"
                                                    />
                                                    <p className="prog-game-name">
                                                        $999,999,999.00
                                                    </p>
                                                    <div className="ctaCardProg text-center">
                                                        <a
                                                            href="#"
                                                            className="btn btn-success"
                                                        >
                                                            PLAY NOW
                                                        </a>
                                                        <a href="#" className>
                                                            PLAY DEMO
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="progressive-column col-6">
                                                <div className="progressive-list ctaProg">
                                                    <img
                                                        src="assets/img/games/progressive/prog-1.png"
                                                        className="img-fluid progressive-thumbnails"
                                                    />
                                                    <p className="prog-game-name">
                                                        $999,999,999.00
                                                    </p>
                                                    <div className="ctaCardProg text-center">
                                                        <a
                                                            href="#"
                                                            className="btn btn-success"
                                                        >
                                                            PLAY NOW
                                                        </a>
                                                        <a href="#" className>
                                                            PLAY DEMO
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="progressive-column col-6">
                                                <div className="progressive-list ctaProg">
                                                    <img
                                                        src="assets/img/games/progressive/prog-2.png"
                                                        className="img-fluid progressive-thumbnails"
                                                    />
                                                    <p className="prog-game-name">
                                                        $999,999,999.00
                                                    </p>
                                                    <div className="ctaCardProg text-center">
                                                        <a
                                                            href="#"
                                                            className="btn btn-success"
                                                        >
                                                            PLAY NOW
                                                        </a>
                                                        <a href="#" className>
                                                            PLAY DEMO
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="vip-home">
                    <div className="container-fluid">
                        <div className="row text-center">
                            <div
                                className="col-md-7 vip-section"
                                style={{ cursor: "pointer" }}
                                onclick="window.location='#';"
                            >
                        
                                <div className="vip-card home-slot">
                                    {" "}
                                    <img
                                        className="card-img-top"
                                        src="assets/img/home/vip-bg.png"
                                        alt="Card image"
                                        style={{ width: "100%" }}
                                    />
                                    <div className="ribbon-top-img-overlay">
                                        {" "}
                                        <img
                                            src="assets/img/home/vip-ribbon.png"
                                            className="vip-ribbon-1"
                                        />{" "}
                                    </div>
                                    <div className="ribbon-bot-img-overlay">
                                        {" "}
                                        <img
                                            src="assets/img/home/vip-ribbon-bottom.png"
                                            className="vip-ribbon-2"
                                        />{" "}
                                    </div>
                                    <div className="logo-top-img-overlay">
                                        {" "}
                                        <img
                                            src="assets/img/home/logo-vip.png"
                                            className="vip-logo img-fluid"
                                        />{" "}
                                    </div>
                                    <div className="title-top-img-overlay">
                                        {" "}
                                        <img
                                            src="assets/img/home/vip-title.png"
                                            className="vip-title img-fluid"
                                        />{" "}
                                    </div>
                                    <div className="title-top-img-overlay">
                                        {" "}
                                        <img
                                            src="assets/img/home/vip-desc.png"
                                            className="vip-desc img-fluid"
                                        />{" "}
                                    </div>
                                    <div className="card-img-overlay">
                                        <p className="vip-text" />
                                        <a href="#" className="btn btn-success">
                                            DETAILS
                                        </a>{" "}
                                    </div>
                                </div>
                            </div>
                            {/* /.col-md-6 sub */}
                            <div className="col-md-5">
                                <div className="card home-feature">
                                    <table
                                        align="center"
                                        style={{ width: "100%" }}
                                    >
                                        <tbody>
                                            <tr>
                                                <td colSpan={2}>
                                                    <img
                                                        src="assets/img/home/feature.png"
                                                        className="img-fluid feature-title"
                                                    />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div className="safe-feature">
                                                        {" "}
                                                        <a>
                                                            <p className="text-feature">
                                                                <img src="assets/img/home/safe-access.png" />{" "}
                                                                Safe Access
                                                            </p>
                                                        </a>{" "}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div className="banking-feature">
                                                        {" "}
                                                        <a href>
                                                            <p className="text-feature">
                                                                <img src="assets/img/home/banking-info.png" />{" "}
                                                                Banking Info
                                                            </p>
                                                        </a>{" "}
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div align="left">
                                        <p className="deposit-feature">
                                            Deposit
                                        </p>
                                    </div>
                                    <div>
                                        <div className="progress customProgress">
                                            <div
                                                className="progress-bar progress-bar-striped  bg-success progress-bar-animated"
                                                role="progressbar"
                                                aria-valuenow={50}
                                                aria-valuemin={0}
                                                aria-valuemax={100}
                                                style={{ width: "50%" }}
                                            />
                                        </div>
                                        <div className="depo-minute">
                                            <p>
                                                <strong>5</strong>/Minute
                                            </p>
                                        </div>
                                    </div>
                                    <div align="left">
                                        <p className="deposit-feature">
                                            Withdrawal
                                        </p>
                                    </div>
                                    <div>
                                        <div className="progress customProgress">
                                            <div
                                                className="progress-bar progress-bar-striped  bg-warning progress-bar-animated"
                                                role="progressbar"
                                                aria-valuenow={90}
                                                aria-valuemin={0}
                                                aria-valuemax={100}
                                                style={{ width: "75%" }}
                                            />
                                        </div>
                                        <div className="depo-minute">
                                            <p>
                                                <strong>9</strong>/Minute
                                            </p>
                                        </div>
                                    </div>
                            
                                </div>
                            </div>
                        </div>
                        {/* /.col-md-6 sub */}
                    </div>
                    {/* .container */}
                    <div className="allPromos-home">
                        <div className="title-livecasino" align="center">
                            <a href=".promo.html">
                                {" "}
                                <img src="assets/img/home/PROMOTIONSS.png" />{" "}
                            </a>
                        </div>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-4 promoFeed">
                                    <a href />
                                    <div className="card promoCard">
                                        <a href>
                                            <div
                                                className="promoImage"
                                                style={{
                                                    backgroundImage:
                                                        'url("assets/img/home/demo1.jpg")'
                                                }}
                                            >
                                       
                                            </div>
                                        </a>
                                        <div className="promoExcerpt">
                                            <a href />
                                            <div className="row">
                                                <a href />
                                                <div className="col-md-7">
                                                    <a href />
                                                    <h3>
                                                        <a href />
                                                        <a href="#">
                                                            Cash Out!
                                                        </a>
                                                    </h3>
                                                    <p>
                                                        Cash out your bets at
                                                        any time r bets at any
                                                        tim
                                                    </p>
                                                </div>
                                                <div className="col-md-5">
                                                    {" "}
                                                    <a
                                                        name
                                                        id
                                                        className="btn btn-success btn-sm btn-block mt-md-3 mb-2"
                                                        href="#"
                                                        role="button"
                                                    >
                                                        Details
                                                    </a>{" "}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* /promoFeed */}
                                <div className="col-md-4 promoFeed">
                                    <a href />
                                    <div className="card promoCard">
                                        <a href>
                                            <div
                                                className="promoImage"
                                                style={{
                                                    backgroundImage:
                                                        'url("assets/img/home/demo2.jpg")'
                                                }}
                                            >
                                             
                                             
                                            </div>
                                        </a>
                                        <div className="promoExcerpt">
                                            <a href />
                                            <div className="row">
                                                <a href />
                                                <div className="col-md-7">
                                                    <a href />
                                                    <h3>
                                                        <a href />
                                                        <a href="#">
                                                            Live Roullete
                                                        </a>
                                                    </h3>
                                                    <p>
                                                        Cash out your bets at
                                                        any time r bets at any
                                                        tim
                                                    </p>
                                                </div>
                                                <div className="col-md-5">
                                                    {" "}
                                                    <a
                                                        name
                                                        id
                                                        className="btn btn-success btn-sm btn-block mt-md-3 mb-2"
                                                        href="#"
                                                        role="button"
                                                    >
                                                        Details
                                                    </a>{" "}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* /promoFeed */}
                                <div className="col-md-4 promoFeed">
                                    <a href />
                                    <div className="card promoCard">
                                        <a href>
                                            <div
                                                className="promoImage"
                                                style={{
                                                    backgroundImage:
                                                        'url("assets/img/home/demo3.jpg")'
                                                }}
                                            >
                                             
                                            </div>
                                        </a>
                                        <div className="promoExcerpt">
                                            <a href />
                                            <div className="row">
                                                <a href />
                                                <div className="col-md-7">
                                                    <a href />
                                                    <h3>
                                                        <a href />
                                                        <a href="#">
                                                            Cash Out!
                                                        </a>
                                                    </h3>
                                                    <p>
                                                        Cash out your bets at
                                                        any time r bets at any
                                                        tim
                                                    </p>
                                                </div>
                                                <div className="col-md-5">
                                                    {" "}
                                                    <a
                                                        name
                                                        id
                                                        className="btn btn-success btn-sm btn-block mt-md-3 mb-2"
                                                        href="#"
                                                        role="button"
                                                    >
                                                        Details
                                                    </a>{" "}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* /promoFeed */}
                            </div>
                            {/* /row */}
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Home;
