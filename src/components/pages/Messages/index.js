import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { withTranslation } from "react-i18next";

import classNames from "classnames";

import {
  getMessages,
} from "../../../actions/player";

class Messages extends Component {

    constructor(props) {
        super(props);
        this.state = {
            amount: "0.00",
            sender: "",
            provider: "",
            success: false,
            error: false,
            validated: false,
            messages: []
        };
    }

    onRead = messageID => {
        console.log(messageID);
    };

    componentDidMount(){
        this.props.getMessages();
    }

    componentWillReceiveProps(nextProps){
        this.setState(nextProps.player);
    }

    render() {
        let { messages } = this.state;

        return (
            <div className="formContent">
                <div className="formContentWrap pl-5 pr-5">
                    <div className="message_inbox row">
                        <div className="col-md-12"><h2 className="formTitle">Messages</h2></div>
                        <div className="col-md-12">
                            <div className="form-group row filter-history no-gutters">
                            </div>
                            <table className="table transaction-history">
                                <thead className="thead-dark">
                                    <tr>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Message</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                { messages.length > 0 ?
                                        (<tbody>
                                            { messages.map((message) => {
                                                return (
                                                    <tr>
                                                        <td>{message.DateCreated}</td>
                                                        <td>{message.Type}</td>
                                                        <td
                                                            className={classNames({
                                                                "font-weight-bold": message.Status === 1
                                                            })}>{message.Subject}</td>
                                                            <td><a
                                                                href="#"
                                                                className={classNames("btn btn-success",{
                                                                    "message_id_1_link": message.Status === 1
                                                                })}
                                                                onClick={this.onRead(1)}>READ</a>
                                                            </td>
                                                        </tr>)
                                            }) }
                                        </tbody>) :
                                        (<tbody className="text-center">
                                        <tr>
                                            <td colSpan="12">
                                                <h4>No Results Found</h4>
                                            </td>
                                        </tr>
                                    </tbody>)
                                }
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

Messages.propTypes = {
  getMessages: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  player: state.player,
  errors: state.player.errors,
  success: state.player.result
});

export default connect(
  mapStateToProps,
  { getMessages }
)(withTranslation()(withRouter(Messages)));
