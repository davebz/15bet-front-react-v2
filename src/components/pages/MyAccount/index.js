import React, { Component } from "react";
import Sidebar from "../../layouts/Sidebar";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import Deposit from "../Deposit";
import Withdrawal from "../Withdrawal";
import Transfer from "../Transfer";
import Profile from "../Profile";
import Bonus from "../Bonus";
import History from "../History";
import Messages from "../Messages";
import {
  resetTransaction,
} from "../../../actions/transaction";
import Error from "../../../errors"



class MyAccount extends Component {
  state = {}
  components = {
    deposit: Deposit,
    withdrawal: Withdrawal,
    transfer: Transfer,
    profile: Profile,
    history: History,
    messages: Messages,
    bonus: Bonus
  };

  static getDerivedStateFromProps(nextProps, prevState){
    if(nextProps.location.pathname.includes('history')){
      nextProps.resetTransaction();
    }
    return true;
  }

  render() {
    const Account = this.components[this.props.match.params.any];

    return (
      <div id="contentWrap">
        <div className="container-fluid">
          <div id="myAccount" className="row">
            <aside className="col-md-2">
              <Sidebar />
            </aside>
           
            <Error>
            <section className="col-md-10">
              <Account />
              </section>
            </Error>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
});

export default connect(
  mapStateToProps,
  { resetTransaction }
)(withRouter(MyAccount));
