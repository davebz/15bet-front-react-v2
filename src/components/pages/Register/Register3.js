import React, { Component } from "react";
import { Link } from "react-router-dom";

class Register3 extends Component {
    componentDidMount(){
        localStorage.removeItem("code");
        localStorage.removeItem("smscode");
        localStorage.removeItem("players");
    }
    render(){
        return(<section id="contentWrap">
            <div className="container">
                <div className="row formContent">
                    <img src="assets/img/welcome-banner.jpg" alt className="img-fluid" />
                    <div className="col-md-12">
                        <form action>
                            <div className="text-center pb-5">
                                <div className="mb-3">
                                    <h1 className="text3D">Welcome to 15BET</h1>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <p className="desc">Congratulations, you have successful registered account with 15BET. <br />
                                        You may now procced to "Deposit" and start enjoy our exciting games.
                                    </p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4 offset-4">
                                    <Link to="/myaccount/deposit" className="btn btn-success btn-lg btn-block mb-3">Deposit Now</Link>
                                    or  <Link to="/myaccount/profile">Continue to My Account Page</Link>
                                </div>
                            </div>
                        </div> {/* .formContentWrap */}
                    </form>
                </div>
            </div>
        </div>
    </section>)
    }
}

export default Register3;
