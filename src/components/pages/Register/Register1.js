import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import classNames from "classnames";
import { getCurrencies, postRegister } from "../../../actions/registration";

class Register1 extends Component {
    state = {
        validated: false,
        username: "",
        password: "",
        confirm_password: "",
        currency: "2",
        phone: "",
        country: "",
        isShown: false,
    };

    componentDidMount(){
        this.props.getCurrencies();
    }

    componentDidUpdate(){
    }

    mapPhoneCode = currencies => {
        const phoneCodes = {};
        currencies.map(currency => {
            return (phoneCodes[currency.CurrencyID] = currency.country.PhoneCode)
        });
        this.setState({ phoneCodes });
    };

    //set default country code and currency
    //if only one currency record from api
    defaultCurrency(){
        if(this.props.currencies.length === 1){
            let key = Object.keys(this.state.phoneCodes)
            return [key[0], this.state.phoneCodes[key[0]]]
        }else{
            return [this.state.currency, this.state.country]
        }
    }

    static getDerivedStateFromProps(nextProps, prevState){
        return { ...nextProps }
    }

    shouldComponentUpdate(nextProps, nextState){
        if(!this.state.hasOwnProperty('phoneCodes')){
            this.mapPhoneCode(this.state.currencies);
            return false;
        }
        return true;
    }

    togglePassword(){
        this.setState({ isShown: !this.state.isShown });
    }

    onSubmit(e){
        e.preventDefault();
        e.stopPropagation();
        const [currency, country] = this.defaultCurrency();
        const { history } = this.props;
        const register = {
            username: this.state.username.replace(/\s/g, ""),
            password: this.state.password.replace(/\s/g, ""),
            confirm_password: this.state.confirm_password.replace(/\s/g, ""),
            phone: this.state.phone,
            country,
            currency,
            history
        };
        this.setState({ validated: true });
        if(this.form.checkValidity()){
           this.props.postRegister(register);
           this.setState({ validated: false });
        }

    }
    onChange = e => {
        //console.log(e.target.name + ' -- ' + e.target.value);

        if (e.target.name === "currency") {
            this.setState({ country: this.state.phoneCodes[e.target.value] });
        }

        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render(){
        const { currencies, error, success } = this.state;

        return (
            <section id="contentWrap">
                <div className="container">
                    <div className="row formContent">
                        <div className="col-md-5 formBG" />
                        <div className="col-md-7">
                            <form
                                className={this.state.validated ? "needs-validation was-validated": ""}
                                noValidate
                                method="post"
                                onSubmit={this.onSubmit.bind(this)}
                                ref={(form)=> this.form = form}>
                                <div className="formContentWrap">
                                    <div className="mb-3"><h1 className="h2">Create an Account</h1></div>

                                    <div className="row mb-3">
                                        <div className="col-12"><h6>Personal Info</h6></div>
                                        <div className="col-md-6">
                                            <label htmlFor="regID">ID<span className="red">*</span></label>
                                            <div className="input-group">
                                                <input
                                                    type="text"
                                                    className={`form-control light form-control-sm ${error && error.username ? "is-invalid":""} `}
                                                    id="regID"
                                                    placeholder
                                                    onChange={this.onChange}
                                                    name="username"
                                                    required/>
                                                <div className="invalid-tooltip">{error && error.username ? error.username: "The ID field is required."}</div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <label htmlFor="currency">Select Currency</label>
                                            <div className="input-group">
                                                <select
                                                    className={`form-control light form-control-sm ${error && error.currency ? "is-invalid":""} `}
                                                    name="currency"
                                                    onChange={this.onChange}
                                                    disabled={currencies.length === 1 ? 'disabled': false}
                                                    required
                                                    id="currency">
                                                    { currencies && currencies.length > 1 ?
                                                    <option
                                                        key={currencies.length+1}
                                                        value={""}
                                                        defaultValue
                                                    ></option>: ""
                                                    }
                                                    {currencies.map(currency => (
                                                        <option
                                                            key={currency.CurrencyID}
                                                            value={currency.CurrencyID}
                                                            defaultValue={currencies.length === 1 ? true: false}
                                                        >
                                                            {currency.Abbreviation}
                                                        </option>
                                                    ))}
                                                </select>
                                                <div className="invalid-tooltip">{error && error.username ? error.currency: "The currency field is required."}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col-md-6">
                                            <label htmlFor="password">Password<span className="red">*</span></label>
                                            <div className="input-group">
                                                <div className="input-group" id="show_hide_password">
                                                    <input
                                                        type={!this.state.isShown ? 'password' : 'text'}
                                                        className={`form-control light form-control-sm ${error && error.password ? "is-invalid":""} `}
                                                        name="password"
                                                        onChange={this.onChange}
                                                        id="password"
                                                        placeholder
                                                        required/>
                                                    <div className="invalid-tooltip">{error && error.password ? error.password[0]: "The password field is required."}</div>
                                                    <div class="input-group-addon">
                                                        <div style={{'cursor':'pointer'}} onClick={this.togglePassword.bind(this)}>
                                                            <i class={classNames('fa',{
                                                                'fa-eye-slash' : !this.state.isShown,
                                                                'fa-eye' : this.state.isShown
                                                            })}
                                                            aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <label htmlFor="confirm_password">Verify Password<span className="red">*</span></label>
                                            <div className="input-group">
                                                <div className="input-group" id="show_hide_password">
                                                    <input
                                                        type={!this.state.isShown ? 'password' : 'text'}
                                                        className={`form-control light form-control-sm ${error && error.password? "is-invalid":""} `}
                                                        name="confirm_password"
                                                        onChange={this.onChange}
                                                        id="confirm_password"
                                                        placeholder
                                                        required
                                                    />
                                                    <div className="invalid-tooltip">{error && error.password && error.password.length > 1 ? error.password[1]: "The password field is required."}</div>
                                                    <div class="input-group-addon">
                                                        <div style={{'cursor':'pointer'}} onClick={this.togglePassword.bind(this)}>
                                                            <i class={classNames('fa',{
                                                                'fa-eye-slash' : !this.state.isShown,
                                                                'fa-eye' : this.state.isShown
                                                            })}
                                                            aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col-md-6">
                                            <label htmlFor="phone">Mobile No.<span className="red">*</span></label>
                                            <div className="input-group-prepend">
                                                <select className="form-control light form-control-sm col-3 noBorderRightTop" id="flag" disabled>
                                                    <option>{ this.state.phoneCodes ? "+" + this.state.phoneCodes[this.state.currency]: "" }</option>
                                                </select>
                                                <input
                                                    type="tel"
                                                    className={`form-control light form-control-sm col noBorderLeftTop ${error && error.phone ? "is-invalid":""} `}
                                                    name="phone"
                                                    onChange={this.onChange}
                                                    id="phone"
                                                    required
                                                />
                                                <div className="invalid-tooltip" style={{"margin-top": "-15px"}}>{error && error.phone ? error.phone: "The phone field is required."}</div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="ref_code">Partner Code</label>
                                                <input
                                                    type="text"
                                                    className={`form-control light form-control-sm ${error && error.partner_code ? 'is-invalid':''} `}
                                                    id="ref_code"
                                                    onChange={this.onChange}
                                                    name="ref_code"
                                                    placeholder />
                                                <div className="invalid-tooltip">
                                                    Invalid ID
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div className="col-12">
                                        <div className="form-group mt-3">
                                            <input
                                                type="checkbox"
                                                className="form-check-input"
                                                id="exampleCheck1"
                                                required
                                            />
                                            <label
                                                className="form-check-label noStyle"
                                                htmlFor="exampleCheck1"
                                            >
                                            I Accept the{" "}
                                            <a href="#tnc">
                                                Terms and Conditions
                                            </a>
                                            ,
                                            <a href="#privacy">
                                                Privacy Policy
                                            </a>
                                            ,{" "}
                                            <a href="#protection">
                                                Player Protection
                                            </a>{" "}
                                            and confirm that I am over 18
                                            years of age.
                                        </label>
                                        </div>
                                    </div>
                                    <button
                                        type="submit"
                                        name
                                        id
                                        className="btn btn-primary btn-lg btn-block"
                                    >
                                        REGISTER
                                    </button>
                                    <div className="col-md-12 mt-5">
                                        <div className="text-right">
                                            <img
                                                src="assets/img/18PlusSSSL.png"
                                                alt=""
                                            />
                                        </div>
                                    </div>
                                </div>{" "}
                                {/* .formContentWrap */}
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = state => ({
    currencies: state.register.currencies,
    error: state.register.errors,
    success: state.register.result,
});

export default connect(
    mapStateToProps,
    { getCurrencies, postRegister }
)(withRouter(Register1));
