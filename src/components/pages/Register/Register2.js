import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { postVerify } from "../../../actions/registration";
import ReactCodeInput from "./ReactCodeInput";

import classNames from "classnames";

import Register3 from "./Register3";

class Register2 extends Component {
    state = {
        smscode: "",
        page: false,
        errors: false
    };

    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

     onSubmit = e => {
         e.preventDefault();
         const players = JSON.parse(localStorage.getItem("players"));
         const smscode = localStorage.getItem("smscode");
         const vip = process.env.REACT_APP_VIP;

         const register = {
             ...players,
             code: smscode,
             code2: localStorage.getItem("code"),
             vip: vip
         };

         this.props.postVerify(register);
     };

    shouldComponentUpdate(nextProps, nextState){
        try {
            if (nextProps.auth.isAuthenticated && !this.props.auth.isAuthenticated) {
                localStorage.removeItem("players");
            }
        } catch(err){
            alert('something went wrong');
            return false;
        }
        return true;
    }

    render(){
        const { errors } = this.props;
        console.log(errors);

        return (
            <section id="contentWrap">
                <div className="container">
                    <div className="row formContent">
                        <div className="col-md-5 formBG" />
                        <div className="col-md-7">
                            <form onSubmit={this.onSubmit}>
                                <div className="formContentWrap">
                                    <div className="mb-3">
                                        <h1 className="h2">Verify Account</h1>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <h6>Please enter verification code</h6>
                                            <p className="desc">We sent you an access code via SMS for Mobile number verification. <br />
                                        </p>
                                    </div>
                                    <div className="col-md-9 offset-md-2 mt-3 mb-3">
                                        <div className="form-group">
                                            <label htmlFor="partnerID">Enter verification code here or</label>  <a href="#"><u>RESEND CODE</u></a>
                                            <ReactCodeInput type="text" name="smscode" fields={6} />
                                            <div className={classNames("small invalid-tooltip",{
                                                "d-block was-validated": errors && Object.keys(errors).length > 0? true : false
                                            })}
                                            style={{"margin-top":"-16px"}}
                                            >{errors.code}</div>
                                        </div>
                                    </div>
                                    <div className="col-12 mb-2">
                                        <p>You are just a few moment away from joining the 15bet sports and casino.</p>
                                        <p>Please enter your correct verification code so you can validate you account. We will only use your
                                            phone number for the purpose of account validation in accordance with 15bet <a href="#">Privacy
                                            Policy</a>.</p>
                                        </div>
                                    </div>
                                    <button type="submit" name id className="btn btn-success btn-lg btn-block">Verify</button>
                                    <div className="col-md-12 mt-5">
                                        <div className="text-right">
                                            <img src="assets/img/18PlusSSSL.png" alt />
                                        </div>
                                    </div>
                                </div> {/* .formContentWrap */}
                            </form>
                        </div>
                    </div>
                </div>
            </section>);
    }
}

Register2.propTypes = {
    postVerify: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.register.errors
});

export default connect(
    mapStateToProps,
    { postVerify }
)(withRouter(Register2));

