import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { withTranslation } from 'react-i18next';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import classNames from "classnames"
import Error from "../../common/error";
import classnames from "classnames";

import {
  getPlayer,
  resetPlayerError,
  postPlayer,
  postPassword,
  postAccount
} from "../../../actions/player";

class Profile extends Component {
  constructor(props) {
    super(props);
    const maxDate = new Date();
    maxDate.setDate( maxDate.getDate() - 1 );
    maxDate.setFullYear( maxDate.getFullYear() - 18 );

    this.state = {
      currency: "",
      country: "",
      name: "",
      birthday: "",
      email: "",
      prefix: "",
      phone: "",
      gender: "",
      bank: "",
      accountNumber: "",
      xxxx:"",
      yyyy:"",
      newAccountNumber: "",
      banks: [],
      errors: {},
      success: {},
      isBank: false,
      updateBank: false,
      bankID: "",
      bankName: "",
      isSubmitted: false,
      isSubmitted2: false,
      isSubmitted3: false,

      errors1: false,
      errors2: false,
      errors3: false,

      success1: false,
      success2: false,
      success3: false,

      type: 'password',
      type2: 'password',
      type3: 'password',
      maxDate
    };

    this.showHide = this.showHide.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

    handleChange(date){
        this.setState({
            birthday: date
        })
    }

  componentDidMount() {
      this.props.getPlayer();

      this.props.history.listen((location, action) => {
          if(location.pathname.includes("profile")){
              this.props.resetPlayerError();
          }
      });
  }


  componentWillReceiveProps(nextProps) {
    const { isSubmitted, isSubmitted2, isSubmitted3 } = this.state;

    const errors = nextProps.errors;
    const errors1 = ["name", "email", "phone", "birthday"].some(k => Object.keys(errors).indexOf(k) > 0)
    const errors2 = ["newAccountNumber", "accountNumber"].some(k => Object.keys(errors).indexOf(k) > 0)
    const errors3 = ["currentPassword", "password"].some(k => Object.keys(errors).indexOf(k) > 0)
    this.setState({ errors, errors1, errors2, errors3 });

    try {
      // account details
      this.setState({ currency: nextProps.player.currency.Abbreviation });
      this.setState({ country: nextProps.player.address.country.Name });
      this.setState({ prefix: nextProps.player.address.country.PhoneCode });

      if (nextProps.player.FirstName && isSubmitted === false) {
        this.setState({ name: nextProps.player.FirstName });
      }

      if (nextProps.player.Email && isSubmitted === false) {
        this.setState({ email: nextProps.player.Email });
      }

      if (nextProps.player.BirthDate && isSubmitted === false) {
        this.setState({ birthday: new Date(nextProps.player.BirthDate) });
      }

      if (isSubmitted === false) {
        this.setState({ gender: nextProps.player.Gender });
        this.setState({ phone: nextProps.player.contact.Info.substring(2) });
      }

      // bank details

      if (nextProps.player.bank){
        this.setState({ bankName: nextProps.player.bank.bank_details.Name });
        if(isSubmitted2 === false){
          this.setState({ bankID: nextProps.player.bank.BankID });

          this.setState({ accountNumber: nextProps.player.bank.AccountNo });
          this.setState({ xxxx: nextProps.player.bank.AccountNo });

          this.setState({ isBank: true });

          this.setState({ yyyy: nextProps.player.bank.bank_details.Name });
        }
      }

      this.setState({ banks: nextProps.banks });

      if (nextProps.errors.phone) {
        this.setState({ success1: false });
      }

      if (nextProps.errors.name) {
        this.setState({ success1: false });
      }

      if (nextProps.errors.email) {
        this.setState({ success1: false });
      }

      if (nextProps.errors.password) {
        this.setState({ success3: false });
      }

      if (nextProps.errors.currentPassword) {
        this.setState({ success3: false });
      }

      if (nextProps.errors.accountNumber) {
        let { accountNumber, accountName } = nextProps.errors;

          //this.setState({ updateBank: false });
          this.setState({ isBank: true });

        this.setState({ errors: { accountName, accountNumber } });


        this.setState({ success2: false });
      }


      if (nextProps.errors.newAccountNumber) {
        let { newAccountNumber } = nextProps.errors;

          //this.setState({ updateBank: false });
        this.setState({ errors: {  newAccountNumber } });
        this.setState({ success2: false });
      }


      if (nextProps.errors.bankID) {
        this.setState({ isBank: false });
        this.setState({ success2: false });
      }

      if (nextProps.errors.accountName) {
        this.setState({ isBank: false });
        this.setState({ success2: false });
      }

      if (nextProps.success && (isSubmitted || isSubmitted2 || isSubmitted3)) {
        if (nextProps.success.message === "Player details updated succesfully") {
          this.setState({ success1: true });
        }
        if (nextProps.success.message === "Player password updated succesfully") {
          this.setState({ success3: true });
        }

        if (nextProps.success.message === "Player bank account succesfully saved") {
          this.setState({ updateBank: false });
          this.setState({ isBank: true });

          this.setState({ bankName: this.state.bank });
          this.setState({ yyyy: this.state.bank });

          this.setState({ success2: true });

          if (this.state.newAccountNumber) {
            this.setState({ accountNumber: this.state.newAccountNumber });
            this.setState({ xxxx: this.state.newAccountNumber });
          }else{
            this.setState({ xxxx: this.state.accountNumber });
          }
        }
        // alert(nextProps.success);
      }
    } catch(error){
      nextProps.showError();
    }
  }

  showHide = param => e => {
    e.preventDefault();

    switch(param) {
      case "type":
       this.setState({ type: this.state.type === 'input' ? 'password' : 'input' })
        break;
      case "type2":
      this.setState({ type2: this.state.type2 === 'input' ? 'password' : 'input' })
        break;
      default:
        this.setState({ type3: this.state.type3 === 'input' ? 'password' : 'input' })
    }

  }


  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleClick() {
    this.setState({ updateBank: true });
  }

  onSubmit = e => {
    e.preventDefault();

    this.setState({ isSubmitted: true });
     

      this.setState({ validated: true });

      if(this.form.checkValidity()){

        const bday = this.state.birthday;
  
        const player = {
            name: this.state.name,
            birthday: bday.toLocaleString(),
            gender: this.state.gender,
            email: this.state.email,
            phone: this.state.phone,
            prefix: this.state.prefix
        };

          this.props.postPlayer(player);
          this.setState({ validated: false });
      }
  };

  onSubmit2 = e => {
      e.preventDefault();

      this.setState({ isSubmitted2: true });


      let player;

      if(this.state.newAccountNumber){
          player = {
              bankID: this.state.bank ? this.state.bank : this.state.bankName,
              accountNumber: this.state.accountNumber,
              accountName: this.state.name,
              newAccountNumber: this.state.newAccountNumber
          };
      }else{
          player = {
              bankID: this.state.bank ? this.state.bank : this.state.bankName,
              accountNumber: this.state.accountNumber,
              accountName: this.state.name,

          };
      }


      this.setState({ bankName: this.state.bank });


      this.setState({ validated2: true });
      if(this.form2.checkValidity()){
          this.props.postAccount(player);
          this.setState({ validated2: false });
      }
  };

    onSubmit3 = e => {
        e.preventDefault();

        this.setState({ isSubmitted3: true });

        const player = {
            currentPassword: this.state.currentPassword,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword
        };
        this.setState({ validated3: true });
        if(this.form3.checkValidity()){
            this.props.postPassword(player);
            this.setState({ validated3: false });
        }
    };

  render() {
    const currency = this.state.currency;
    const country = this.state.country;
    const prefix = this.state.prefix;
    const name = this.state.name;
    const email = this.state.email;
    const birthday = this.state.birthday;
    const phone = this.state.phone;
    const gender = this.state.gender;
    const banks = this.state.banks;
    const isBank = this.state.isBank;
    const updateBank = this.state.updateBank;
    const bankName = this.state.bankName;
    const { errors, errors1, errors2, errors3 } = this.state;
    const accountNumber = "**********" + this.state.xxxx.substr(this.state.xxxx.length - 4);

    
    return (
        <div class="formContent">
            <div class="formContentWrap pl-5 pr-5">
                <form
                    noValidate
                    className={classNames({
                        "needs-validation was-validated": this.state.validated || errors1
                    })}
                    onSubmit={this.onSubmit.bind(this)}
                    ref={(form)=> this.form = form}>
                    <div className="row mb-4" id="account-details">
                        <div className="col-md-12">
                            <h2 class="formTitle">Account Details</h2>

                        </div>

                        {this.state.success1 ? (
                            <div className="col-md-12">
                                <div className="alert alert-success" role="alert">
                                    Player details updated succesfully
                                </div>
                            </div>
                        ) : (
                            <div />
                        )}

                        <div className="col-md-4">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label">Username</label>
                                <div className="col-sm-8">
                                    <b className="text_display selected_username">
                                        {localStorage.getItem("playerName")}
                                    </b>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label">Currency</label>
                                <div className="col-sm-8">
                                    <b className="text_display selected_lang">{currency}</b>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label">Country</label>
                                <div className="col-sm-8">
                                    <b className="text_display selected_country">{country}</b>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group row">
                                <label htmlFor="name" className="col-sm-4 col-form-label">
                                    Name <span className="red">*</span>
                                </label>
                                <div className="col-sm-8">
                                    <input
                                        type="text"
                                        className={classNames("form-control", {
                                            "is-invalid": errors.name
                                        })}
                                        name="name"
                                        id="name"
                                        placeholder="Zilong Legend"
                                        onChange={this.onChange}
                                        value={name}
                                        required
                                    />
                                    <Error errors={errors} id="name"/>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group row">
                                <label htmlFor="email" className="col-sm-4 col-form-label">
                                    Email <span className="red">*</span>
                                </label>
                                <div className="col-sm-8">
                                    <input
                                        type="email"
                                        name="email"
                                        id="email"
                                        className={classNames("form-control", {
                                            "is-invalid": errors.email
                                        })}
                                        placeholder="Email"
                                        onChange={this.onChange}
                                        value={email}
                                        required
                                    />
                                    <Error errors={errors} id="email"/>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group row">
                                <label htmlFor="birthday" className="col-sm-4 col-form-label">
                                    Birthday <span className="red">*</span>
                                </label>
                                <div className="col-sm-8">
                                    <DatePicker
                                        className={classNames("form-control", {
                                            "is-invalid": errors.birthday
                                        })}
                                        selected={this.state.birthday}
                                        onChange={this.handleChange}
                                        showYearDropdown
                                        showMonthDropdown
                                        dateFormat="dd-MM-yyyy"
                                        maxDate={this.state.maxDate}
                                        placeholderText="dd-mm-yyyy"
                                        id="birthday"
                                        required 
                                    />

                                    
                                    {/* <Error errors={errors} id="birthday"  />  */}
                                    <div  className={classnames("invalid-tooltip", {
                                        "d-block": this.state.birthday === ''  && this.state.validated 
                                            })}
                                        >
                                        The birthday field is required.
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group row">
                                <label htmlFor="contact" className="col-sm-4 col-form-label">
                                    Contact Number <span className="red">*</span>
                                </label>
                                <div className="col-sm-8">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <select className="form-control form-control-sm" id="flag" required>
                                                <option>+{prefix}</option>
                                            </select>
                                        </div>
                                        <input
                                            type="phone"
                                            name="phone"
                                            className={classNames("form-control form-control-sm", {
                                                "is-invalid": errors.phone
                                            })}
                                            placeholder="Contact Number"
                                            onChange={this.onChange}
                                            value={phone}
                                            required
                                        />
                                        <Error errors={errors} id="phone"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group row">
                                <label htmlFor="gender" className="col-sm-4 col-form-label">
                                    Gender <span className="red">*</span>
                                </label>
                                <div className="col-sm-8">
                                    <div className="form-check form-check-inline">
                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            name="gender"
                                            id="genderM"
                                            value="m"
                                            onChange={this.onChange}
                                            checked={gender === "m"}
                                        />
                                        <label
                                            className="form-check-label"
                                            htmlFor="genderM"
                                        >
                                            Male
                                        </label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            name="gender"
                                            id="genderF"
                                            value="f"
                                            onChange={this.onChange}
                                            checked={gender === "f"}
                                        />
                                        <label
                                            className="form-check-label"
                                            htmlFor="genderF"
                                        >
                                            Female
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div className="col-md-12">
                            <div className="action-btn-modal">
                                <button type="submit" className="btn btn-success float-right">
                                    SAVE
                                </button>
                            </div>
                        </div>
                    </div>
                </form>

        <form
            noValidate
            className={classNames({
                "needs-validation was-validated": this.state.validated2 || errors2
            })}
            onSubmit={this.onSubmit2.bind(this)}
            ref={(form)=> this.form2 = form}>
            <div className="row mb-4 bank-details">
                <div className="col-md-12">
                    <h2 class="formTitle">Bank Details</h2>
                </div>

                    {this.state.success2 ? (
                        <div className="col-md-12">
                            <div className="alert alert-success" role="alert">
                                Player bank account succesfully saved
                            </div>
                        </div>
                    ) : (
                        <div />
                    )}

                    {isBank ? (
                        <React.Fragment>
                            {updateBank ? (
                                <div className="col-md-12">
                                    <div class="row">
                                        <div className="col-sm-5">
                                            Bank Name: &nbsp;
                                            <b className="text_display selected_username">
                                                {this.state.yyyy}
                                            </b>
                                        </div>

                                        <div className="col-sm-5">
                                            Account Number: &nbsp;
                                            <b className="text_display selected_lang">
                                                {accountNumber}
                                            </b>
                                        </div>
                                    </div>

                                    <div className="row add-new-bank-details">
                                        <h3 className="col-md-12 mt-5">Update Bank Account</h3>
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label
                                                    htmlFor="bankName"
                                                    className="col-form-label"
                                                >
                                                    Bank Name <span className="red">*</span>
                                                </label>
                                                <select
                                                    className="custom-select"
                                                    name="bank"
                                                    id="bankName"
                                                    onChange={this.onChange}
                                                    defaultValue={bankName}
                                                    required
                                                >
                                                    <option value="">Select Bank</option>
                                                    {banks.map(bank => (
                                                        <option
                                                            key={bank.bank.BankID}
                                                            value={bank.bank.Name}
                                                        >
                                                            {bank.bank.Name}
                                                        </option>
                                                    ))}
                                                </select>
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label
                                                    htmlFor="newAccountNumber"
                                                    className="col-form-label"
                                                >
                                                    New Account # <span className="red">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    id="newAccountNumber"
                                                    name="newAccountNumber"
                                                    className={classNames("form-control", {
                                                        "is-invalid": errors.newAccountNumber
                                                    })}
                                                    placeholder="New Account #"
                                                    onChange={this.onChange}
                                                    required
                                                />
                                                <Error errors={errors} id="newAccountNumber" />
                                            </div>
                                        </div>

                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label
                                                    htmlFor="accountNumber"
                                                    className="col-form-label"
                                                >
                                                    Verify Old Account #<span className="red">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    id="accountNumber"
                                                    name="accountNumber"
                                                    className={classNames("form-control", {
                                                        "is-invalid": errors.accountNumber
                                                    })}
                                                    placeholder={accountNumber}
                                                    onChange={this.onChange}
                                                    required
                                                />
                                                <Error errors={errors} id="accountNumber" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                <div class="col-md-12 user-bank-details">
                                    <div class="row">
                                        <div className="col-sm-5">
                                            Bank Name: &nbsp;
                                            <b className="text_display selected_username">
                                                {this.state.yyyy}
                                            </b>
                                        </div>

                                        <div className="col-sm-5">
                                            Account Number: &nbsp;
                                            <b className="text_display selected_lang">
                                                {accountNumber}
                                            </b>
                                        </div>
                                        <div className="col-sm-2 text-right">
                                            <div className="action-btn-modal">
                                                <button className="btn btn-success btn-remove-bank" onClick={this.handleClick}>
                                                    UPDATE
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </React.Fragment>
                    ) : (
                        <React.Fragment>
                            <div className="col-md-6">
                                <div className="form-group row">
                                    <label
                                        htmlFor="bankName"
                                        className="col-sm-4 col-form-label"
                                    >
                                        Bank Name <span className="red">*</span>
                                    </label>
                                    <div className="col-sm-8">
                                        <select
                                            className="custom-select"
                                            name="bank"
                                            id="bankName"
                                            onChange={this.onChange}
                                            required
                                        >
                                            <option value="">Select Bank</option>
                                            {banks.map(bank => (
                                                <option
                                                    key={bank.bank.BankID}
                                                    value={bank.bank.Name}
                                                >
                                                    {bank.bank.Name}
                                                </option>
                                            ))}
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group row">
                                    <label
                                        htmlFor="accountNumber"
                                        className="col-sm-4 col-form-label"
                                    >
                                        Account #<span className="red">*</span>
                                    </label>
                                    <div className="col-sm-8">
                                        <input
                                            type="text"
                                            id="accountNumber"
                                            name="accountNumber"
                                            className="form-control"
                                            placeholder="***-***-***21"
                                            onChange={this.onChange}
                                            required
                                        />
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    )}
                    {isBank && updateBank === false ? (
                        <div></div>
                    ) : (
                        <div className="col-md-12">
                            <div className="action-btn-modal">
                                <button type="submit" className="btn btn-success float-right" disabled={name ? false: true}>
                                    SAVE
                                </button>
                            </div>
                        </div>
                    )}
                </div>
            </form>

            <form
                noValidate
                onSubmit={this.onSubmit3.bind(this)}
                ref={(form)=> this.form3 = form}
                className={classNames({
                    "needs-validation was-validated": this.state.validated3 || errors3
                })}>
                <div className="row mb-3" id="password">
                    <div className="col-md-12">
                        <h2 className="formTitle">Change Password</h2>
                    </div>

                        {this.state.success3 ? (
                            <div className="col-md-12">
                                <div className="alert alert-success" role="alert">
                                    Player password updated succesfully
                                </div>
                            </div>
                        ) : (
                            <div />
                        )}

                        <div className="col-md-12">
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label
                                            htmlFor="currentPassword"
                                            className="col-form-label"
                                        >
                                            Current Password <span className="red">*</span>
                                        </label>
                                        <div className="input-group">
                                            <input
                                                type={this.state.type}
                                                className="form-control"
                                                className={classNames("form-control", {
                                                    "is-invalid": errors.currentPassword
                                                })}
                                                name="currentPassword"
                                                id="currentPassword"
                                                placeholder="Current Password"
                                                onChange={this.onChange}
                                                required
                                            />
                                            <Error errors={errors} id="currentPassword" />
                                            <div className="input-group-addon" onClick={this.showHide("type")} style={{ "cursor": "pointer" }}>
                                                <i className={classNames("fa",{
                                                    "fa-eye-slash" : this.state.type === "password",
                                                    "fa-eye" : this.state.type === "input"
                                                })}></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label htmlFor="password" className="col-form-label">
                                            New Password <span className="red">*</span>
                                        </label>
                                        <div className="input-group">
                                            <input
                                                type={this.state.type2}
                                                className={classNames("form-control", {
                                                    "is-invalid": errors.password
                                                })}
                                                name="password"
                                                id="password"
                                                placeholder="New Password"
                                                onChange={this.onChange}
                                                required
                                            />
                                            <Error errors={errors} id="password"/>
                                            <div className="input-group-addon" onClick={this.showHide("type2")} style={{ "cursor": "pointer" }}>
                                                <i className={classNames("fa",{
                                                    "fa-eye-slash" : this.state.type2 === "password",
                                                    "fa-eye" : this.state.type2 === "input"
                                                })}></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label
                                            htmlFor="confirmPassword"
                                            className="col-form-label"
                                        >
                                            Re-enter Password <span className="red">*</span>
                                        </label>
                                        <div className="input-group">
                                            <input
                                                type={this.state.type3}
                                                className={classNames("form-control", {
                                                    "is-invalid": errors.password
                                                })}
                                                name="confirmPassword"
                                                placeholder="Re-enter New Password"
                                                onChange={this.onChange}
                                                required
                                            />
                                            <Error errors={errors} id="password"/>

                                            <div className="input-group-addon" onClick={this.showHide("type3")} style={{ "cursor": "pointer" }}>
                                                <i className={classNames("fa",{
                                                    "fa-eye-slash" : this.state.type3 === "password",
                                                    "fa-eye" : this.state.type3 === "input"
                                                })}></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="action-btn-modal">
                                <button type="submit" className="btn btn-success float-right">
                                    SAVE
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    );
  }
}

Profile.propTypes = {
  getPlayer: PropTypes.func.isRequired,
  postPlayer: PropTypes.func.isRequired,
  postAccount: PropTypes.func.isRequired,
  postPassword: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  player: state.player.player,
  banks: state.player.player.bank_currency,
  errors: state.player.errors,
  success: state.player.result
});

export default connect(
  mapStateToProps,
  { getPlayer, postPlayer, postPassword, postAccount, resetPlayerError }
)(withTranslation()(withRouter(Profile)));
