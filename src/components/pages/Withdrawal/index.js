import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import classnames from "classnames";
import {
    postWithdrawal,
    checkWithdrawal,
    getBankAccount
} from "../../../actions/payment";
import Wallet from "../../layouts/Wallet";

class Withdrawal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: 0,
            sender: "",
            provider: "",
            success: false,
            bankName: "",
            bankAccountName: "",
            bankAccountNumber: "",
            errors: false,
            isSubmitted: false,
            received: 0,
            validated: false,
        };
    }

    componentDidMount() {
        this.props.getBankAccount();
        this.props.checkWithdrawal();
    }

    componentWillReceiveProps(nextProps) {
        try {
            if (nextProps.withdrawal) {
                this.setState({
                    bankName: nextProps.withdrawal.bank_details.Name
                });
                this.setState({
                    bankAccountName: nextProps.withdrawal.AccountName
                });
                this.setState({
                    bankAccountNumber: nextProps.withdrawal.AccountNo
                });
            }

            if (nextProps.errors.amount) {
                this.setState({ error: true });
                this.setState({ success: false });
            }

            if (nextProps.success.length > 0) {
                this.setState({ error: false });
                this.setState({ success: true });
            }

            const { isSubmitted } = this.state;
            if (isSubmitted === false) {
                let allin = nextProps.wallet.wallet;
                if (allin) {
                    allin = allin.replace(/,/g, "");
                    allin = parseFloat(allin);
                    this.setState({
                        amount: allin
                            .toFixed(2)
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    });
                    this.setState({
                        received: allin
                            .toFixed(0)
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    });
                }
            }
        } catch (err) {
            nextProps.showError();
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({ isSubmitted: true });

        const payment = {
            amount: Number(this.state.amount.replace(/[^0-9-.]/g, ""))
        };

        this.setState({ validated: true });
        if(this.form.checkValidity()){
            this.props.postWithdrawal(payment);
            this.setState({ validated: false });
        }

        
    };

    quickAddAmount = (amountToAdd) => {
        let currentAmount = this.state.amount;
        if (currentAmount) {
            currentAmount = Number(currentAmount.replace(/[^0-9-.]/g, ""));
        }
        if (String(currentAmount).match(/[a-z]/i)) {
            currentAmount = 0;
        }
        currentAmount = parseFloat(currentAmount) + parseFloat(amountToAdd);
        currentAmount = currentAmount
            .toFixed(2)
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        this.setState({ amount: currentAmount });
    };

    refreshPage = () => {
        window.location.reload();
    };

    render() {
        const accountNumber = "**********" + this.state.bankAccountNumber.substr(this.state.bankAccountNumber.length - 4);
        const { pendingWithdrawal, errors } = this.props;
        const amount = this.state.amount;
        let received = 0;

        if (amount !== 0) {
            received = amount.substring(0, amount.length - 3);
        }

        return (
            
            <form
                className={this.state.validated ? "needs-validation was-validated": ""}
                noValidate method="post"
                onSubmit={this.onSubmit.bind(this)}
                ref={(form)=> this.form = form}>

                <div className="formContent">
                    <div className="formContentWrap pl-5 pr-5">
                        <div className="row">
                            {this.state.success || pendingWithdrawal ? (
                                <div
                                    className="col-md-7"
                                    id="deposit-message"
                                >
                                    <h2 className="formTitle">
                                        Withdrawal Sent!
                                    </h2>
                                    <p>
                                        Lorem ipsum dolor sit amet,
                                        consectetur adipiscing elit, sed do
                                        eiusmod tempor incididunt ut labore
                                        et dolore magna aliqua. Vestibulum
                                        rhoncus est pellentesque elit
                                        ullamcorper dignissim cras
                                        tincidunt. Nunc lobortis mattis
                                        aliquam faucibus purus in. Quam
                                        elementum pulvinar etiam non quam
                                        lacus. Ipsum nunc aliquet bibendum
                                        enim facilisis gravida neque. Mollis
                                        nunc sed id semper risus in. Diam ut
                                        venenatis tellus in metus vulputate
                                        eu scelerisque felis. Sed enim ut
                                        sem viverra aliquet eget sit amet.
                                        Egestas fringilla phasellus faucibus
                                        scelerisque. Nunc vel risus commodo
                                        viverra maecenas accumsan lacus vel
                                        facilisis. Mauris pharetra et
                                        ultrices neque ornare aenean euismod
                                        elementum.
                                    </p>
                                </div>
                            ) : (
                                <div className="col-md-7" id="withdrawal-form">
                                    <h2 className="formTitle">
                                        Withdrawal
                                    </h2>

                                    <div className="form-group row">
                                        <label
                                            htmlFor="inputEmail3"
                                            className="col-sm-4 col-form-label"
                                        >
                                            Payment Method
                                        </label>
                                        <div className="col-sm-8">
                                            <select
                                                className="custom-select"
                                                disabled
                                            >
                                                <option
                                                    value="Local Transfer"
                                                    defaultValue
                                                >
                                                    Local Transfer
                                                </option>
                                            </select>
                                            <small
                                                id="emailHelp"
                                                className="form-text text-muted"
                                            >
                                                (Min: 10000.00 / Max:
                                                100000.00)
                                            </small>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <div className="col-md-12">
                                            <div className="btn-quick-amount withraw-quick-amount">
                                                <button
                                                className="btn-default btn-add-amount"
                                                type="button"
                                                onClick={this.quickAddAmount.bind(this, 30000)}
                                                >
                                                +30K
                                                </button>
                                                <button
                                                className="btn-default btn-add-amount"
                                                type="button"
                                                onClick={this.quickAddAmount.bind(this, 100000)}
                                                >
                                                +100K
                                                </button>
                                                <button
                                                className="btn-default btn-add-amount"
                                                type="button"
                                                onClick={this.quickAddAmount.bind(this, 500000)}
                                                >
                                                +500K
                                                </button>
                                                <button
                                                className="btn-default btn-add-amount"
                                                type="button"
                                                onClick={this.quickAddAmount.bind(this, 1000000)}
                                                >
                                                +1M
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group row">
                                        <label
                                            htmlFor="amount"
                                            className="col-sm-4 col-form-label"
                                        >
                                            Withdrawal Amount
                                        </label>
                                        <div className="col-sm-8">
                                        <input
                                            type="text"
                                            id="amount"
                                            name="amount"
                                            className={classnames("form-control", { "is-invalid": errors && errors.amount })}
                                            placeholder="100,000"
                                            onChange={this.onChange}
                                            value={amount}
                                            required
                                        />
                                            <small
                                                id="emailHelp"
                                                className="form-text text-muted"
                                            >
                                                    (Min: 50,000.00)
                                            </small>
                                            
                                            <div className={classnames("invalid-tooltip", { "d-block": errors && errors.amount })} >
                                                {errors && errors.amount ? errors.amount : 'The withdrawal amount field is required.'}
                                            </div>
                                        </div>
                                    </div>


                                    <div className="form-group row">
                                        <label
                                            htmlFor="inputEmail3"
                                            className="col-sm-4 col-form-label"
                                        >
                                            Amount Received
                                        </label>
                                        <div className="col-sm-8">
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder={received}
                                                id="amount_received"
                                                aria-label="Amount Received"
                                                aria-describedby="basic-addon1"
                                                disabled
                                            />
                                            <small
                                                id="emailHelp"
                                                className="form-text text-muted"
                                            >
                                                Witdrawal is only available
                                                for Main Wallet
                                            </small>
                                        </div>
                                    </div>

                                    {this.state.bankName ? (
                                        <div>
                                            <div className="form-group row">
                                                <label
                                                    htmlFor="inputEmail3"
                                                    className="col-sm-4 col-form-label"
                                                >
                                                    Bank Name
                                                </label>
                                                <div className="col-sm-8">
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        value={
                                                            this.state
                                                                .bankName
                                                        }
                                                        disabled
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label
                                                    htmlFor="inputEmail3"
                                                    className="col-sm-4 col-form-label"
                                                >
                                                    Account Name
                                                </label>
                                                <div className="col-sm-8">
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        value={
                                                            this.state
                                                                .bankAccountName
                                                        }
                                                        disabled
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label
                                                    htmlFor="inputEmail3"
                                                    className="col-sm-4 col-form-label"
                                                >
                                                    Account Number
                                                </label>
                                                <div className="col-sm-8">
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        value={
                                                            accountNumber
                                                        }
                                                        disabled
                                                    />
                                                </div>
                                            </div>

                                            <div className="action-btn-modal text-center">
                                                <button
                                                    className="btn btn-default"
                                                    type="reset"
                                                >
                                                    RESET
                                                </button>{" "}
                                                <button
                                                    className="btn btn-success"
                                                    type="submit"
                                                >
                                                    SUBMIT
                                                </button>
                                            </div>
                                        </div>
                                    ) : (
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div
                                                    className="alert alert-danger"
                                                    role="alert"
                                                >
                                                    No Bank Account Yet
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                </div>
                            )}

                            <Wallet />
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

Withdrawal.propTypes = {
    postWithdrawal: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    errors: state.payment.withdrawErrors,
    success: state.payment.withdrawResult,
    withdrawal: state.payment.transaction.account,
    pendingWithdrawal: state.payment.pendingWithdrawal,
    wallet: state.auth.wallet
});

export default connect(
    mapStateToProps,
    { postWithdrawal, getBankAccount, checkWithdrawal }
)(withRouter(Withdrawal));
