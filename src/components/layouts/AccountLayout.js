import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import classnames from "classnames";
import Sidebar from "./Sidebar";

class AccountLayout extends Component {
  constructor(){
      super();
  }
  render() {
      console.log(this.props.children);
      let { children } = this.props;
      console.log(children);
    return (
        <div>
            {children}
        </div>
    );
  }
}

export default withRouter(AccountLayout);
