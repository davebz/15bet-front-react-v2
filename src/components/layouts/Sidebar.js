import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import classnames from "classnames";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getWallet } from "../../actions/player";

const routes = [
    { path: "deposit", name: "Deposit" },
    { path: "withdrawal", name: "Withdrawal" },
    { path: "transfer", name: "Transfer" },
    { path: "history", name: "Transaction History" },
    { path: "bonus", name: "Bonus / Coupon" },
    { path: "profile", name: "Profile" },
    { path: "messages", name: "Messages" }
];

class Sidebar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            page: ""
        };
    }

    componentDidMount() {
        this.props.getWallet();
    }

    componentWillMount() {
        this.unlisten = this.props.history.listen((location) => {
            this.setState({page: location.pathname});
        });
        
        this.setState({page: this.props.location.pathname});
      
      }

    componentWillUnmount() {
        this.unlisten();
    }
    
    render() {
        const vip = this.props.vip;
        const routeComponents = routes.map(({ path, name }) => (
            <Link to={`/myaccount/${path}`} className={classnames("list-group-item list-group-item-action", {
                active:  this.state.page === `/myaccount/${path}`
            })} key={name}>
            {name}
            </Link>
        ));

        return (
            
                <div className="sticky">
                    <div className="profileCard">
                        <div className="row">
                            <div className="col-3"><i className="fa fa-medal" aria-hidden="true"></i></div>
                            <div className="col-9">
                                <h3>Welcome, {localStorage.playerName}</h3>
                                <p className="desc">VIP Level: <span className="levelType">{vip}</span></p>
                            </div>
                            </div>
                        </div>

                    <div className="list-group dark">
                    {routeComponents}
                    </div>
                </div>
           
        )
    }
}

Sidebar.propTypes = {
    getWallet: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    vip: state.auth.wallet.vip,
});

export default connect(
    mapStateToProps,
    { getWallet }
)(withRouter(Sidebar));
