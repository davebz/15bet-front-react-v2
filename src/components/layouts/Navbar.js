import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withTranslation } from 'react-i18next';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

class Nav extends Component {
    render() {
        const { t } = this.props;
        return (
            <nav className="row navigation">
                <div className="col-md-12 mobile-none">
                    <ul className="main_nav">
                        <li>
                            
                            <Link to={`/sports`}><i className="fa fa-play-circle blinking" /> {t('15 Sports')}</Link>
                            <ul className="sub-menu">
                                <li>
                                    <Link to={`/sports`}>{t('Double')}</Link>
                                </li>
                                <li>
                                    <Link to={`/sports2`}>{t('Double Info')}</Link>
                                </li>
                                <li>
                                    <Link to={`/sports3`}>{t('Single')}</Link>
                                </li>
                                <li>
                                    <Link to={`/sports4`}>{t('Single Info')}</Link>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <Link to={`/ow-sports`}>{t('OW Sports')}</Link>
                        </li>
                        <li>
                            <Link to={`/casino`}>{t('Live Casino')} <i className="fa fa-angle-down" /></Link>
                            <div className="megaMenu">
                                {" "}
                                <span className="bgMenu" />
                                <div className="row">
                                    <div className="col-md-3">
                                        <img
                                            src="../assets/img/bg/livecasino.png"
                                            alt=""
                                            className="cashback"
                                        />
                                    </div>

                                    <div className="col-md-9">
                                        <div className="row imgLinks Live-subMenu">
                                            <a
                                                href="http://www.ho-interactive.com"
                                                className="col"
                                            >
                                                <img
                                                    src="../assets/img/Partner/hogaming.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/188.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/ig.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/LEGaming.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/blueprint.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/megawin.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                        </div>
                                        <div className="row imgLinks">
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/stag8.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/pt.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/tgp.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/vr.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/ss.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <Link to={`/slot`}>{t('Slot Games')} <i className="fa fa-angle-down" /></Link>
                            <div className="megaMenu">
                                {" "}
                                <span className="bgMenu" />
                                <div className="row">
                                    <div className="col-md-3">
                                        <img
                                            src="../assets/img/bg/slotgames.png"
                                            alt=""
                                            className="cashback"
                                        />
                                    </div>

                                    <div className="col-md-9">
                                        <div className="row imgLinks">
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/188.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/ig.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/LEGaming.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/blueprint.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                        </div>
                                        <div className="row imgLinks">
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/stag8.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/pt.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/tgp.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/vr.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <Link to={`/mini-games`}>
                                {t('Mini Games')} <i className="fa fa-angle-down" />
                            </Link>
                            <div className="megaMenu">
                                {" "}
                                <span className="bgMenu" />
                                <div className="row">
                                    <div className="col-md-3">
                                        <img
                                            src="../assets/img/bg/mini-games.png"
                                            alt=""
                                            className="cashback"
                                        />
                                    </div>

                                    <div className="col-md-9">
                                        <div className="row imgLinks-minigames">
                                            <a href="#" className="imgTitle">
                                                <img
                                                    src="../assets/img/bg/fishhunter-placeholder.png"
                                                    alt=""
                                                />{" "}
                                                Fish Hunter
                                            </a>
                                            <a href="#" className="col" />
                                            <a href="#" className="col" />
                                            <a
                                                href="#"
                                                className="col-md-2 imgTitle"
                                            >
                                                <img
                                                    src="../assets/img/bg/p2p-placeholder.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                                P2P
                                            </a>
                                            <a href="#" className="col" />
                                        </div>
                                        <div className="row imgLinks">
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/stag8.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/pt.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/tgp.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/vr.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                            <a href="#" className="col">
                                                <img
                                                    src="../assets/img/Partner/ss.png"
                                                    alt=""
                                                    className="img-fluid"
                                                />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            
                            <Link to={`/promo`}>{t('Promotion')} <i className="fa fa-angle-down" /></Link>

                            <div className="megaMenu">
                                {" "}
                                <span className="bgMenu">a</span>
                                <div className="row imgLinks-promo">
                                    <a href="#" className="col-md-2 offset-1">
                                        <img
                                            src="../assets/img/promotion/sports.png"
                                            alt=""
                                            className="img-fluid"
                                        />
                                    </a>
                                    <a href="#" className="col-md-2">
                                        <img
                                            src="../assets/img/promotion/live-casino.png"
                                            alt=""
                                            className="img-fluid"
                                        />
                                    </a>
                                    <a href="#" className="col-md-2">
                                        <img
                                            src="../assets/img/promotion/slot-games.png"
                                            alt=""
                                            className="img-fluid"
                                        />
                                    </a>

                                    <a href="#" className="col-md-2">
                                        <img
                                            src="../assets/img/promotion/fish-hunter.png"
                                            alt=""
                                            className="img-fluid"
                                        />
                                    </a>
                                    <a href="#" className="col-md-2">
                                        <img
                                            src="../assets/img/promotion/p2p.png"
                                            alt=""
                                            className="img-fluid"
                                        />
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <Link to={`/partners`}>{t('Partners')}</Link>
                        </li>
                        <li>
                            <Link to={`/mobile`}>{t('Mobile')}</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default connect(
)(withTranslation()(withRouter(Nav)));
