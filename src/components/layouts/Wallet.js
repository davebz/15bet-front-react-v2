import React, { Component } from "react";

class Wallet extends Component {
  render() {
    return (
        <div className="col-md-5">
            <div className="panel all-wallet">
                <div className="panel-heading text-right">
                    <h4 className="wallet_total">
                        <strong className="float-left"> TOTAL:  </strong>
                        <span>0</span>
                    </h4>
                    <h5 className="wallet_balance">
                        <strong className="float-left"> MAIN WALLET:</strong>
                        <span>0</span>
                    </h5>
                    <span className="refresh" onClick={this.refreshPage}  >
                        <i className="fa fa-sync" />
                    </span>
                </div>

                <div className="wallet-scroll">
                    <ul className="list-group wallet-info">
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/megawin.png" alt="" /><strong>Megawin: </strong></span>  200,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/LEGaming.png" alt="" /><strong>LE Gaming: </strong></span>  300,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/ky.png" alt="" /><strong>KY: </strong></span> 25,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/kg.png" alt="" /><strong>KENO Gaming: </strong></span> 250,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/jdb.png" alt="" /><strong>JDB: </strong></span> 100,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/cq9.png" alt="" /><strong>CQ9: </strong></span>  200,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/stag8.png" alt="" /><strong>Stag 8: </strong></span>  300,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/spg.png" alt="" /><strong>Spade Gaming: </strong></span> 25,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/sg.png" alt="" /><strong>SG Win: </strong></span> 250,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/ag.png" alt="" /><strong>AG Gaming: </strong></span> 100,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/cmd.png" alt="" /><strong>CMO 368: </strong></span>  200,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/ig.png" alt="" /><strong>IG Gaming: </strong></span>  300,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/esport.png" alt="" /><strong>E Sports: </strong></span> 25,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/188.png" alt="" /><strong>188 Bet: </strong></span> 250,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/hc.png" alt="" /><strong>HC Esport: </strong></span> 100,000</li>
                        <li className="list-group-item"><span className="label float-left"><img src="../assets/img/Partner/im_logo.png" alt="" /><strong>Inplay Matrix: </strong></span>  300,000</li>
                    </ul> 
                </div>
            </div>
        </div>
    );
  }
}

export default Wallet;
