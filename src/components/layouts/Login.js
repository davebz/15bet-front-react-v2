import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authentication";
import { Link } from "react-router-dom";
import {Button} from "reactstrap";
import { withRouter } from "react-router-dom";
import classnames from "classnames";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            opacity: false,
            dropdownOpen: false,
            login_name: "",
            password: "",
            errors: {},
            validated: false,
        };
    }

    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    // onSubmit = e => {
    //     const { history } = this.props;
    //     e.preventDefault();

    //     if (this.state.login_name === "") {
    //         this.setState({ error: "ID is required" });
    //         return;
    //     }

    //     if (this.state.password === "") {
    //         this.setState({ error: "Password is required" });
    //         return;
    //     }

    //     const user = {
    //         login_name: this.state.login_name,
    //         password: this.state.password,
    //         deviceUrl: window.location.href
    //     };
    //     this.props.loginUser({ user, history});
    // };

    onSubmit = (e) => {
        const { history } = this.props;
        e.preventDefault();
        e.stopPropagation();

       

        this.setState({ validated: true });
        if(this.form.checkValidity()){
            const user = {
                login_name: this.state.login_name,
                password: this.state.password,
                deviceUrl: window.location.href
            };
            
            this.props.loginUser({ user, history });
            this.setState({ validated: false });
        }
        
        
    };

    componentDidMount() {}

    render() {
        const  errors   = this.props.errors;
        
        
        return (
            <React.Fragment>
                <div className="col-md-5 col-sm-12">
                <form className={classnames("form-inline float-right", {
                    "was-validated": this.state.validated
                    })}
                    onSubmit={this.onSubmit}
                    noValidate
                    ref={(form)=> this.form = form}
                >

                        <div className="form-group mb-md-3 mt-md-2">
                            <label htmlFor="playerID" className="sr-only">ID</label>
                            <input
                            type="text"
                            className="form-control form-control-sm light"
                            id="playerID"
                            placeholder="ID"
                            name="login_name"
                            onChange={this.onChange}
                            required
                            />
                           
                            <div  className={classnames("invalid-tooltip", {
                                    "d-block": errors && errors.login_name
                                })}
                            >
                            {errors && errors.login_name ? errors.login_name : 'The ID field is required.'}
                            </div>
                        </div>

                        <div className="form-group mb-md-3 mt-md-2 ml-md-1">
                            <label htmlFor="partnerPassword" className="sr-only">Password</label>
                            <input
                            type="password"
                            className="form-control form-control-sm light"
                            id="partnerPassword"
                            placeholder="Password"
                            name="password"
                            onChange={this.onChange}
                            required
                            />

                            <div className={classnames("invalid-tooltip", {
                                    "d-block": errors && errors.password
                                })}
                            >
                            {errors && errors.password ? errors.password : 'The password field is required.'}
                            </div>
                        </div>

                        <div className="form-group mb-md-3 mt-md-2 ml-md-1">
                            <Button type="submit" className="btn btn-secondary btn-sm btn-login">
                                Login
                            </Button>
                        </div>
                    </form>

                </div>

                <div className="col-md-2">
                    <Link to={`/register`} className="btn btn-success btn-block mt-md-1 ml-md-2 registerBtn">Register Now</Link>
                </div>

            </React.Fragment>
        );
    }
}

Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.auth.errors.errors,
    success: state.auth.player
});

export default connect(
    mapStateToProps,
    { loginUser }
)(withRouter(Login));


