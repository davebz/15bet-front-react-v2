import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authentication";
import { getWallet } from "../../actions/player";
import { withRouter } from "react-router-dom";

class LoginHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wallet: "",
            currency: ""
        };
    }
    
    onLogout = e => {
        e.preventDefault();
        this.props.logoutUser(this.props.history);
    };

    refreshWallet = e => {
        e.preventDefault();
        this.props.getWallet();
    };
    
    componentDidMount() {
        this.props.getWallet();
    }

    render() {
        let wallet = this.props.wallet;
        if (wallet) {
            wallet = wallet.slice(0, -3);
        }

        return (
            <div className="col-md-7 text-right">
                <div className="accountLinks">
                    <div className="walletDetais inline">
                        <i className="fa fa-medal"></i> {localStorage.playerName}
                    </div>
                    <div className="walletBalance inline">
                        {this.props.currency} <span className="availBalance">{wallet}</span>
                        <a href="#" onClick={this.refreshWallet.bind(this)} ><i className="fa fa-sync text-warning small"></i></a>
                    </div>
                    <div className="quickLinks inline">
                        <Link to={`/myaccount/profile`}>My Account</Link>
                        <Link to={`/myaccount/transfer`}>Transfer</Link>
                        <Link to={`/myaccount/withdrawal`}>Withdrawal</Link>
                        <Link to={`/myaccount/deposit`}>Deposit</Link>
                        <Link to={`/`} className="logout text-white" onClick={this.onLogout.bind(this)}>Logout <i className="fas fa-sign-out-alt" /></Link>
                    </div>
                </div>
            </div>
        );
    }
}

LoginHeader.propTypes = {
    logoutUser: PropTypes.func.isRequired,
    getWallet: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    wallet: state.auth.wallet.wallet,
    currency: state.auth.wallet.currency
});

export default connect(
    mapStateToProps,
    { logoutUser, getWallet }
)(withRouter(LoginHeader));
