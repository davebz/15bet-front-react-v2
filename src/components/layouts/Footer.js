import React, { Component } from "react";
import { Link } from "react-router-dom";
import Pagcor from "../../assets/img/Paggcor.png";
import Gambleaware from "../../assets/img/GA18.png";
import GamingCuracao from "../../assets/img/gc-logo.png";
import QRimage from "../../assets/img/download_mobile.png";

class Footer extends Component {
    render() {
        return (
            <footer className="footer-wrap">
                <div className="container-fluid">
                    <div className="row  align-items-top">
                        <div className="col-md-9">
                            <div className="row footer-links">
                                <nav className="col-md-3">
                                    <h3>Quick Links</h3>
                                    <ul>
                                        <li>
                                            <Link to="/about">About Us</Link>
                                        </li>
                                        <li>
                                            <Link to="/betting-rules">
                                                Betting Rules
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/privacy-policy">
                                                Privacy Policy
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/responsible-gaming">
                                                Responsible Gaming
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/terms">
                                                Terms and Conditions
                                            </Link>
                                        </li>
                                    </ul>
                                </nav>

                                <nav className="col-md-3">
                                    <h3>Help</h3>
                                    <ul className="row">
                                        <li className="col-12">
                                            <Link to="/deposit">
                                                Deposit &amp; Windrawal
                                            </Link>
                                        </li>
                                        <li className="col-12">
                                            <Link to="/faq">FAQs</Link>
                                        </li>
                                    </ul>
                                </nav>
                                <nav className="col-md-6">
                                    <h3>Products</h3>
                                    <ul className="row">
                                        <li className="col-md-6">
                                            <Link to="/">Home</Link>
                                        </li>
                                        <li className="col-md-6">
                                            <Link to="/sports">
                                                15bet Sports
                                            </Link>
                                        </li>
                                        <li className="col-md-6">
                                            <Link to="/asian">OW Sports</Link>
                                        </li>
                                        <li className="col-md-6">
                                            <Link to="/casino">
                                                Live Casino
                                            </Link>
                                        </li>
                                        <li className="col-md-6">
                                            <Link to="/slots">Slots Games</Link>
                                        </li>
                                        <li className="col-md-6">
                                            <Link to="/e-sports">E-Sports</Link>
                                        </li>
                                        <li className="col-md-6">
                                            <Link to="/mini">Mini Games</Link>
                                        </li>
                                        <li className="col-md-6">
                                            <Link to="/promotion">
                                                Promotion
                                            </Link>
                                        </li>
                                        <li className="col-md-6">
                                            <Link to="/partners">Partners</Link>
                                        </li>
                                        <li className="col-md-6">
                                            <Link to="/mobile">Mobile</Link>
                                        </li>
                                        <li className="col-md-6">
                                            <Link to="/vip">VIP</Link>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <h3>Mobile version available</h3>
                            <a href="#">
                                <img src={QRimage} alt="QR-Code" />
                            </a>

                            <Link
                                to="/download-instruction"
                                className="btn btn-primary btn-sm mt-3"
                            >
                                Download Instruction
                            </Link>
                        </div>
                    </div>

                    <div className="row rowSupport">
                        <div className="col-md-9">
                            <h3>Responsible Gaming</h3>
                            <p className="gaming_logos">
                                <Link to="/">
                                    <img
                                        src={Pagcor}
                                        alt="Paggcor"
                                        width="50px"
                                    />
                                </Link>
                                <a
                                    href="https://licensing.gaming-curacao.com/validation/?lh=45414685181c57f0abc9125fbb5b1d6d"
                                    alt="Achilles Business Holding Limited"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        src={GamingCuracao}
                                        width="150px"
                                        alt="Gaming Curacao"
                                    />
                                </a>
                                <Link to="/">
                                    <img src={Gambleaware} alt="Gambleaware" />
                                </Link>
                            </p>
                        </div>
                        <div className="col-md-3">
                            <h3>Support</h3>
                            <a href="mailto:cskr@15bet.com">
                                <i
                                    className="far fa-envelope"
                                    aria-hidden="true"
                                />{" "}
                                cskr@15bet.com
                            </a>
                        </div>
                    </div>
                </div>

                <div className="copyright">
                    <div className="container-fluid">
                        15BET is located in the Philippines has obtained the
                        Philippine PAGCOR legal network gaming license, all
                        follow the Philippines Internet gambling rules. We also
                        registered in Gaming Curacao licensing and supervision
                        conducted by Gaming Services Provider NV, as authorized
                        by the Government of Curacao under National Decree No
                        14, dated 18 August 1998. Copyright 2019
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
