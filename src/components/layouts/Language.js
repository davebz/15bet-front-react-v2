import React, { Component } from "react";
import { withTranslation } from 'react-i18next';
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

class Langauge extends Component {
  state = {
    dropdownOpen: false
  };

  toggle = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  render() {
    const { i18n } = this.props;
    const changeLanguage = (lng) => {
      i18n.changeLanguage(lng);
    };

    return (
      <Dropdown
        isOpen={this.state.dropdownOpen}
        toggle={this.toggle}
        className="show " 
      >
        <DropdownToggle  id="dropdownMenuLink" caret>
            <div className={i18n.language === 'en-US' ? '': 'd-none'}>
              <img src="../assets/img/en.png" alt="EN" /> English
            </div>
            <div className={i18n.language === 'ko-KR' ? '': 'd-none'}>
              <img src="../assets/img/kr.png" alt="KR" /> Korea
            </div>    
        </DropdownToggle>

        <DropdownMenu>
          <DropdownItem  onClick={() => changeLanguage('en-US')} >
            <img src="../assets/img/en.png" alt="EN" /> English
          </DropdownItem>
          <DropdownItem  onClick={() => changeLanguage('ko-KR')}>
            <img src="../assets/img/kr.png" alt="KR" /> Korea
          </DropdownItem>

        </DropdownMenu>
      </Dropdown>
    );
  }
}

export default withTranslation()(Langauge);
