import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Login from "./Login";
import LoginHeader from "./LoginHeader";
import Navbar from "./Navbar";
import Clock from "react-live-clock";
import logo from "../../assets/img/15bet_logo.svg";
import Moment from 'react-moment';
import Langauge from "./Language";

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            opacity: false,
            dropdownOpen: false
        };
        this.toggle = this.toggle.bind(this);
    }

    componentWillMount = () => {
        window.addEventListener("scroll", this.handleScroll);
    };

    componentWillUnmount = () => {
        window.removeEventListener("scroll", this.handleScroll);
    };

    handleScroll = () => {
        let scroll = window.pageYOffset || this.props.scrollTop;
        let opacityHeader = 300;

        if (scroll >= opacityHeader) {
            this.setState({ opacity: true });
        } else {
            this.setState({ opacity: false });
        }
    };

    toggle() {
        this.setState((prevState) => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }

    render() {

        const token = localStorage.getItem("appToken");

        return (
            <header id="header" className={this.state.opacity ? "opacity" : ""}>
                <div className="container-fluid">
                    <div className="row top-section">
                        <div className="col-md-12">
                            <div className="top-section-content row float-right">
                            {token ? 
                            (<div className="action_icons inline">
                                 <Link to={`/myaccount/bonus`} className="nav-link btn-bonus"> 
                                    <i className="fa fa-bell">
                                        <span />
                                     </i>
                                </Link>
 
                                <Link to={`/myaccount/messages`} className="nav-link btn-messages"> 
                                    <i className="fa fa-ticket-alt">
                                        { this.props.message > 0 ? (<span class="badge badge-danger">1</span> ) : (<span />) }
                                     </i>
                                </Link>
                            </div>) :('')}

                                <div className="currentDate inline text-right">
                                    <Clock format={"MMMM DD, YYYY | HH:mm"} />{" "}(GMT <Moment format="ZZ"></Moment>)
                                </div>

                                <div className="text-right lang_select inline">
                                    <Langauge />
                                </div>

                                <div className="inline">
                                    <div className="chat">
                                        <div className="dropdown_chat">
                                            <span>
                                                <i className="fa fa-comments dropbtn" />{" "}
                                                HELP
                                            </span>
                                            <div className="dropdown-content">
                                                <h3>Support Center</h3>
                                                <a
                                                    href="#"
                                                    className="btn btn-primary mb-1 btn-block"
                                                >
                                                    <i className="fa fa-headset" />{" "}
                                                    Live Chat
                                                </a>
                                                <a
                                                    href="#"
                                                    className="btn btn-default mb-1 btn-block"
                                                >
                                                    <i className="fa fa-envelope" />{" "}
                                                    cskr@15bet.com
                                                </a>
                                                <a
                                                    href="#"
                                                    className="btn btn-default mb-1 btn-block"
                                                >
                                                    <i className="fa fa-paper-plane" />{" "}
                                                    +6309171515151
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row no-gutters mt-2">
                        <div className="logo col-md-5 col-sm-10">
                            <Link to={`/`}> <img src={logo} className="img-fluid" alt="15 Bet Logo"/></Link>
                        </div>

                        {token ? <LoginHeader /> : <Login />}

                    </div>
                    <Navbar />
                </div>
            </header>
        );
    }
}


Header.propTypes = {
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    message: state.auth.wallet.message,
});

export default connect(
    mapStateToProps
)(withRouter(Header));
  