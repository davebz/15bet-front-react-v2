import React from "react";
import classNames from "classnames";

const Error = ({errors, id}) => {
    
    const error_msg = {
        name: "This field is required.",
        email: "Valid email is required.",
        phone: "This field is required.",
        password: "This field is required.",
        currentPassword: "This field is required.",
        newAccountNumber: "This field is required.",
        accountNumber: "This field is required.",
        birthday: "This field is required.",
    }
    let style;

    switch(id){
        case "password":
        case "currentPassword":
            style = { "margin-top": "0px" }
            break;
        case "newAccountNumber":
        case "accountNumber":
            style = { "margin-top": "-15px" }
            break;
        case "birthday":
            style = { "margin-top": "-5px" }
            break;
        default:
            style = { "margin-top": "-5px" }
    }

    // on change password error
    // edge case dont display error for current password
    if(id === "currentPassword" && (errors["password"] && !errors["password"].includes("match."))){
        return (<></>)
    }

   
    
    return (
        <div
            className={
                classNames("invalid-tooltip",{
                    "d-block": errors.hasOwnProperty("currentPassword") && id === "currentPassword" 
                })
            }
            style={style}>
            { errors[id] ? errors[id]: error_msg[id] }
        </div>

        
    )
}
export default Error;
