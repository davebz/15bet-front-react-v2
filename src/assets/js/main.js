// var scene = document.getElementById('scene');
// var parallax = new Parallax(scene);
import jQuery from "jquery";
jQuery(document).ready(function() {
    jQuery("header").peelback({
        adImage: "../assets/img/home/peel-ad.png",
        peelImage: "../assets/img/home/peel-image.png",
        clickURL: "https://html.15b3t.com/new/vip.html",
        smallSize: 100,
        bigSize: 500,
        gaTrack: false,
        gaLabel: "VIP",
        autoAnimate: true
    });
});
