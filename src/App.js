import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import routes from "./routes";
import './i18n';

import Header from "./components/layouts/Header";
import Footer from "./components/layouts/Footer";
import NotFound from "./components/pages/NotFound";
import { PrivateRoute } from "./Auth";
import { CircleArrow as ScrollUpButton } from "react-scroll-up-button";

import "./assets/js/jquery.peelback";
import "./assets/js/main";

class App extends Component {
    render() {
        const routeComponents = routes.map(({ path, component, auth }, key) =>
            !auth ? (
                <Route exact path={path} component={component} key={key} />
            ) : (
                <PrivateRoute exact path={path} component={component} key={key} />
            )
        );

        return (
            <Provider store={store}>
                <Router>
                    <React.Fragment>
                        <Header />
                        <div id="main-wrap">
                            <Switch>
                                {routeComponents}
                                <Route component={NotFound} />
                            </Switch>
                        </div>
                        <Footer />
                        <ScrollUpButton ShowAtPosition={300}  AnimationDuration={1000} ContainerClassName="scroll-up" />
                    </React.Fragment>
                </Router>
            </Provider>
        );
    }
}

export default App;
