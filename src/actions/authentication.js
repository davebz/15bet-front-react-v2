import { GET_USER, DELETE_USER, SET_CURRENT_USER } from "./types";

const loginUser = user => {
  return {
    type: GET_USER,
    payload: user
  };
};

const logoutUser = history => {
  return {
    type: DELETE_USER,
    payload: history
  };
};

const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

export { loginUser, logoutUser, setCurrentUser };
