import {
  GET_BANK_ACCOUNT,
  REQUEST_POST_DEPOSIT,
  REQUEST_POST_WITHDRAWAL,
  REQUEST_WITHDRAWAL_CHECK,
  REQUEST_DEPOSIT_CHECK
} from "./types";

export const postDeposit = payment => {
  return {
    type: REQUEST_POST_DEPOSIT,
    payload: payment
  };
};

export const checkDeposit = () => {
  return {
    type: REQUEST_DEPOSIT_CHECK
  };
};

export const checkWithdrawal = () => {
  return {
    type: REQUEST_WITHDRAWAL_CHECK
  };
};

export const getBankAccount = () => {
  return {
    type: GET_BANK_ACCOUNT
  };
};

export const postWithdrawal = payment => {
  return {
    type: REQUEST_POST_WITHDRAWAL,
    payload: payment
  };
};
