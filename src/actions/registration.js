import {
  GET_BRAND_CURRENCY,
  GET_COUNTRIES,
  SET_CURRENT_USER,
  REQUEST_POST_REGISTER,
  REQUEST_POST_VERIFY
} from "./types";

export const getCurrencies = () => {
  return {
    type: GET_BRAND_CURRENCY
  };
};

export const getCountries = () => {
  return {
    type: GET_COUNTRIES
  };
};

export const postRegister = register => {
  return {
    type: REQUEST_POST_REGISTER,
    payload: register
  };
};

export const postVerify = player => {
  return {
    type: REQUEST_POST_VERIFY,
    payload: player
  };
};

export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};
