import {
  GET_PLAYER,
  GET_MESSAGES,
  PLAYER_RESET_ERRORS,
  REQUEST_POST_PLAYER,
  REQUEST_POST_ACCOUNT,
  REQUEST_POST_PASSWORD,
  GET_WALLET_USER
} from "./types";

export const getPlayer = () => {
  return {
    type: GET_PLAYER
  };
};

export const postPlayer = player => {
  return {
    type: REQUEST_POST_PLAYER,
    payload: player
  };
};

export const postAccount = player => {
  return {
    type: REQUEST_POST_ACCOUNT,
    payload: player
  };
};

export const postPassword = player => {
  return {
    type: REQUEST_POST_PASSWORD,
    payload: player
  };
};

export const getWallet = () => {
  return {
    type: GET_WALLET_USER
  };
};

export const getMessages = () => {
  return {
    type: GET_MESSAGES
  };
}

export const resetPlayerError = () => {
  return {
      type: PLAYER_RESET_ERRORS,
      payload: false
  };
}
