import { GET_BANNERS } from "./types";

export const getBanners = () => {
  return {
    type: GET_BANNERS
  };
};
