import {
  SET_INITIAL_TRANSACTION,
  GET_TRANSACTION_HISTORY,
  SET_TRANSACTION_TYPE,
  SET_TRANSACTION_DATE
} from "./types";

export const getTransactionHistory = () => {
  return { type: GET_TRANSACTION_HISTORY };
};

export const selectType = event => {
  let params = {
    transaction_type: event.target.value,
    type: SET_TRANSACTION_TYPE
  };
  return params;
};

export const selectDate = event => {
  let params = { date: event.target.value, type: SET_TRANSACTION_DATE };
  return params;
};

export const resetTransaction = () => {
  return {
    type: SET_INITIAL_TRANSACTION
  }
}
