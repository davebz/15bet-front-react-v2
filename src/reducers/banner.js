import { SET_BANNERS } from "../actions/types";

const initialState = {
  banners: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_BANNERS:
      return {
        ...state,
        banners: action.payload
      };
    default:
      return state;
  }
}
