import { combineReducers } from "redux";
import authReducer from "./authentication";
import banners from "./banner";
import registerReducer from "./registration";
import playerReducer from "./player";
import paymentReducer from "./payment";
import transactionHistoryReducer from "./transaction";

export default combineReducers({
  auth: authReducer,
  banners: banners,
  register: registerReducer,
  player: playerReducer,
  payment: paymentReducer,
  transactionHistory: transactionHistoryReducer
});
