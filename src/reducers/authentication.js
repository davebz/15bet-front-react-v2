import {
  SET_CURRENT_USER,
  LOGIN_REQUEST,
  GET_LOGIN_SUCCESS,
  GET_ERRORS,
  SET_WALLET_USER
} from "../actions/types";
import isEmpty from "../validation/is-empty";

const initialState = {
  isAuthenticated: false,
  user: {},
  wallet: {},
  player: {},
  errors: {},
  pending: false
};



export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload,
        errors: {},
        pending: false
      };

    case LOGIN_REQUEST:
      return {
        ...state,
        pending: true,
        errors: {}
      };
    case GET_ERRORS:
      return {
        ...state,
        errors: action.payload,
        pending: false
      };
    case GET_LOGIN_SUCCESS:
      return {
        ...state,
        player: action.payload,
        errors: false
      };
    case SET_WALLET_USER:
      return {
        ...state,
        wallet: action.payload,
        errors: {},
        pending: false
      };
    default:
      return state;
  }
}
