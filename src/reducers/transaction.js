import { SET_INITIAL_TRANSACTION, SET_TRANSACTION_HISTORY, SET_TRANSACTION_TYPE, SET_TRANSACTION_DATE } from "../actions/types";

const initialState = {
  transactionHistory: [],
  transactionType: { 1000: "Deposit", 2000: "Withdraw", 3000: "Transfer" },
  transactionStatus: { 1: "Pending", 2: "Rejected", 3: "Processing", 4: "Assign To Risk", 5: "Assign to CS", 6: "Assign to Payment", 7: "Risk Checked", 8: "Risk Processing", 9: "Approved" },
  transactionStatusColor: { 1: "blue", 2: "red", 9: "green" },
  selectedType: "0",
  selectedDate: "0"
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_INITIAL_TRANSACTION:
      return initialState;
    case SET_TRANSACTION_HISTORY:
      return {
        ...state,
        transactionHistory: action.payload
      };
    case SET_TRANSACTION_TYPE:
      return {
        ...state,
        selectedType: action.transaction_type,
      };
    case SET_TRANSACTION_DATE:
      return {
        ...state,
        selectedDate: action.date
      };
    default:
      return state;
  }
}
