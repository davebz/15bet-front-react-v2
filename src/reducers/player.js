import {
    SET_PLAYER,
    PLAYER_SUCCESS,
    PLAYER_RESET_ERRORS,
    PLAYER_ERRORS,
    PASSWORD_SUCCESS,
    PASSWORD_ERRORS,
    BANK_SUCCESS,
    BANK_ERRORS,
    MESSAGES_SUCCESS
} from "../actions/types";

const initialState = {
  player: [],
  result: [],
  errors: [],
  banks: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_PLAYER:
      return {
        ...state,
        player: action.payload
      };
    case MESSAGES_SUCCESS:
      return {
        ...state,
       messages: action.payload
      };
    case PLAYER_SUCCESS:
      return {
        ...state,
        result: action.payload,
        errors: false
      };
    case PLAYER_RESET_ERRORS:
      return {
        ...state,
        errors: action.payload
      };
    case PLAYER_ERRORS:
      return {
        ...state,
        result: false,
        errors: action.payload
      };
    case PASSWORD_SUCCESS:
      return {
        ...state,
        result: action.payload,
        errors: false
      };
    case PASSWORD_ERRORS:
      return {
        ...state,
        result: false,
        errors: action.payload
      };
    case BANK_SUCCESS:
      return {
        ...state,
        result: action.payload,
        errors: false
      };
    case BANK_ERRORS:
      return {
        ...state,
        result: false,
        errors: action.payload
      };
    default:
      return state;
  }
}
