import {
  DEPOSIT_SUCCESS,
  DEPOSIT_ERRORS,
  DEPOSIT_CHECK,
  SET_BANK_ACCOUNT,
  WITHDRAWAL_SUCCESS,
  WITHDRAWAL_ERRORS,
  WITHDRAWAL_CHECK
} from "../actions/types";

const initialState = {
  depositResult: {},
  depositErrors: {},
  withdrawResult: {},
  withdrawErrors: {},
  transaction: {"account":{"BankAccountID":19,"PlayerID":25,"BankID":8,"Branch":"","AccountName":"daveee","AccountNo":"1234512345","CountryID":0,"StateID":0,"CityID":0,"ColorTaggingID":0,"CreatedBy":1,"DateCreated":"2019-03-18 16:35:02","UpdatedBy":1,"DateUpdated":"2019-03-18 16:35:02","Status":"1","IsPrimary":"1","bank_details":{"BankID":8,"Name":"Bank of Beijing","Description":"Bank of Beijing","CreatedBy":1,"DateCreated":"2018-12-03 04:55:04","UpdatedBy":1,"DateUpdated":"2018-12-12 05:30:46","Status":1}}},
  pendingWithdrawal: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case DEPOSIT_SUCCESS:
      return {
        ...state,
        depositResult: action.payload,
        depositErrors: false
      };
    case DEPOSIT_ERRORS:
      return {
        ...state,
        depositResult: false,
        depositErrors: action.payload
      };
    case DEPOSIT_CHECK:
      return {
        ...state,
        transaction: action.payload
      };
    case WITHDRAWAL_SUCCESS:
      return {
        ...state,
        withdrawResult: action.payload,
        withdrawErrors: false
      };
    case WITHDRAWAL_ERRORS:
      return {
        ...state,
        withdrawResult: false,
        withdrawErrors: action.payload
      };
    case WITHDRAWAL_CHECK:
      return {
        ...state,
        pendingWithdrawal: (action.payload.transaction > 0)
      };
    case SET_BANK_ACCOUNT:
      return {
        ...state,
        transaction: action.payload
      };
    default:
      return state;
  }
}
