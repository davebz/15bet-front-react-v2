import {
  SET_BRAND_CURRENCY,
  SET_COUNTRIES,
  REGISTER,
  REGISTER_SET_PAGE,
  VERIFY,
  GET_REGISTER_ERRORS,
  GET_REGISTER2_ERRORS,
  GET_REGISTER_SUCCESS
} from "../actions/types";

const initialState = {
  currencies: [],
  countries: [],
  currentPage: 1,
  result: {},
  errors: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_BRAND_CURRENCY:
      return {
        ...state,
        currencies: action.payload
      };
    case SET_COUNTRIES:
      return {
        ...state,
        countries: action.payload
      };
    case REGISTER:
      return {
        ...state,
        result: action.payload,
        errors: false
      };
    case REGISTER_SET_PAGE:
      return {
        ...state,
        currentPage: action.payload,
      };
    case VERIFY:
      return {
        ...state,
        result: action.payload
      };
    case GET_REGISTER_SUCCESS:
      return {
        ...state,
        result: action.payload,
        errors: false
      };
    case GET_REGISTER_ERRORS:
      return {
        ...state,
        errors: action.payload
      };
    case GET_REGISTER2_ERRORS:
      return {
        ...state,
        errors: action.payload
      };
    default:
      return state;
  }
}
