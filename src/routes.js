import Home from "./components/pages/Home";
//import Casino from "./components/pages/Casino";
//import Slot from "./components/pages/Slot";
import Register from "./components/pages/Register";
//import Mobile from "./components/pages/Mobile";
//import P2P from "./components/pages/P2P";
//import Fishhunter from "./components/pages/Fishhunter";
//import About from "./components/pages/About";
//import Sports from "./components/pages/Sports";
//import Sports2 from "./components/pages/Sports/Sports2";
//import Sports3 from "./components/pages/Sports/Sports3";
//import Sports4 from "./components/pages/Sports/Sports4";
//import Asian from "./components/pages/Asian";
//import Asian2 from "./components/pages/Asian/Asian2";
//import Asian3 from "./components/pages/Asian/Asian3";
//import Asian4 from "./components/pages/Asian/Asian4";
import MyAccount from "./components/pages/MyAccount";
//
const routes = [
  { path: "/", exact: true, name: "Home", component: Home, auth: false },
//  { path: "/casino", exact: true, name: "Casino", component: Casino, auth: false },
//  { path: "/slot", exact: true, name: "Slot", component: Slot, auth: false },
  { path: "/register", exact: true, name: "Register", component: Register, auth: false },
//  { path: "/mobile", exact: true, name: "Mobile", component: Mobile, auth: false },
//  { path: "/p2p", exact: true, name: "P2P", component: P2P, auth: false },
//  { path: "/fishhunter", exact: true, name: "Fishhunter", component: Fishhunter, auth: false },
//  { path: "/About", exact: true, name: "About Us", component: About, auth: false },
//  { path: "/sports", exact: true, name: "Sports", component: Sports, auth: false },
//  { path: "/sports2", exact: true, name: "Sports 2", component: Sports2, auth: false },
//  { path: "/sports3", exact: true, name: "Sports 3", component: Sports3, auth: false },
//  { path: "/sports4", exact: true, name: "Sports 4", component: Sports4, auth: false },
//  { path: "/asian", exact: true, name: "Asian", component: Asian, auth: false },
//  { path: "/asian2", exact: true, name: "Asian 2", component: Asian2, auth: false },
//  { path: "/asian3", exact: true, name: "Asian 3", component: Asian3, auth: false },
//  { path: "/asian4", exact: true, name: "Asian 4", component: Asian4, auth: false },
    { path: "/myaccount/:any", exact: true, name: "My Account", component: MyAccount, auth: true }
];

export default routes;
