import axios from "axios";
import { ROOT_URL } from "../config/api";

export { getBanners };

function getBanners() {
  return axios.get(`${ROOT_URL}/banners?category=sliders`);
}
