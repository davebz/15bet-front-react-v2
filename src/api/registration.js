import axios from "axios";
import { ROOT_URL } from "../config/api";

export { getCurrencies, getCountries, postRegister, postVerify };

const brand = process.env.REACT_APP_BRAND;

function getCurrencies() {
  return axios.get(`${ROOT_URL}/brand?brand=${brand}&status=1`);
}

function getCountries() {
  return axios.get(`${ROOT_URL}/countries`);
}

function postRegister(register) {
  return axios.post(`${ROOT_URL}/register`, register);
}

function postVerify(players) {
  return axios.post(`${ROOT_URL}/verify`, players);
}
