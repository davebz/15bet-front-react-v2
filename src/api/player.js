import axios from "axios";
import { ROOT_URL, findToken } from "../config/api";

export { getWallet, getPlayer, getMessages, postPlayer, postAccount, postPassword }

function getWallet(){
  return axios
    .get(`${ROOT_URL}/wallet`, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }
    });
}

function getPlayer(){
  return axios
    .get(`${ROOT_URL}/user`, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }
    });
}

function getMessages(){
  return axios
    .get(`${ROOT_URL}/messages`, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }
    });
}

function postPlayer(player){
 return axios
    .post(`${ROOT_URL}/player`, player, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }
    })
}

function postAccount(player){
  return axios
    .post(`${ROOT_URL}/bankAccount`, player, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }
    })
}

function postPassword(player){
  return axios
    .post(`${ROOT_URL}/password`, player, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }
    })
}
