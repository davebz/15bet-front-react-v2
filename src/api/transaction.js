import axios from "axios";
import { ROOT_URL, findToken } from "../config/api";

export function getTransactions(params){
  return axios
    .get(`${ROOT_URL}/transactions`, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }, params
    });
}
