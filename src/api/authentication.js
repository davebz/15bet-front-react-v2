import axios from "axios";
import { ROOT_URL } from "../config/api";

export { postUser };

function postUser(params) {
  return axios.post(`${ROOT_URL}/login`, params);
}
