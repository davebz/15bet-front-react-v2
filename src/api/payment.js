import axios from "axios";
import { ROOT_URL, findToken } from "../config/api";

export { postDeposit, postWithdrawal, getBankAccount, checkDeposit, checkWithdrawal }

function postDeposit(payment){
  return axios
    .post(`${ROOT_URL}/deposit`, payment, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }
    })
}

function postWithdrawal(payment){
  return axios
    .post(`${ROOT_URL}/withdrawal`, payment, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }
    })
}

function getBankAccount(){
  return axios
    .get(`${ROOT_URL}/bankAccount`, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }
    })
}

function checkDeposit(){
  return axios
    .get(`${ROOT_URL}/checkPendingTransaction`, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }, params: { transaction_type: 1000 }
    })
}

function checkWithdrawal(){
  return axios
    .get(`${ROOT_URL}/checkPendingTransaction`, {
      headers: {
        Authorization: `Bearer ${findToken()}`
      }, params: { transaction_type: 2000 }
    })
}
