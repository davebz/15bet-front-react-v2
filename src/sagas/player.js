import { takeLatest, put, call } from 'redux-saga/effects';

import {
    getWallet,
    getMessages,
    getPlayer,
    postPlayer,
    postAccount,
    postPassword
} from "../api/player"

import {
    GET_WALLET_USER,
    SET_WALLET_USER,
    SET_PLAYER,
    GET_PLAYER,
    GET_MESSAGES,
    MESSAGES_SUCCESS,
    MESSAGES_ERRORS,
    PASSWORD_SUCCESS,
    PASSWORD_ERRORS,
    PLAYER_SUCCESS,
    PLAYER_ERRORS,
    BANK_SUCCESS,
    BANK_ERRORS,
    REQUEST_POST_ACCOUNT,
    REQUEST_POST_PASSWORD,
    REQUEST_POST_PLAYER,
} from "../actions/types";

export {
    playerWatcher
};

function* playerWatcher() {
    yield takeLatest(GET_WALLET_USER, fetchWallet)
    yield takeLatest(GET_PLAYER, fetchPlayer)
    yield takeLatest(GET_MESSAGES, fetchMessages)
    yield takeLatest(REQUEST_POST_ACCOUNT, fetchPostAccount)
    yield takeLatest(REQUEST_POST_PASSWORD, fetchPostPassword)
    yield takeLatest(REQUEST_POST_PLAYER, fetchPostPlayer)
}

function* fetchWallet() {
    const result = yield call(getWallet);
    yield put({
        type: SET_WALLET_USER,
        payload: result.data
    });
}

function* fetchMessages() {
    const result = yield call(getMessages);
    yield put({
        type: MESSAGES_SUCCESS,
        payload: result.data
    });
}

function* fetchPlayer() {
    const result = yield call(getPlayer);
    yield put({
        type: SET_PLAYER,
        payload: result.data
    });
}

function* fetchPostPlayer(action) {
    try {
        let result = yield call(postPlayer, action.payload);
        yield put({
            type: PLAYER_SUCCESS,
            payload: result.data
        });
    }catch(error){
        yield put({
            type: PLAYER_ERRORS,
            payload: error.response.data.errors
        });
    }
}

function* fetchPostAccount(action) {
    try {
        let result = yield call(postAccount, action.payload);
        yield put({
            type: BANK_SUCCESS,
            payload: result.data
        });
    }catch(error){
        yield put({
            type: BANK_ERRORS,
            payload: error.response.data.errors
        });
    }
}

function* fetchPostPassword(action) {
    try {
        let result = yield call(postPassword, action.payload);
        yield put({
            type: PASSWORD_SUCCESS,
            payload: result.data
        });
    }catch(error){
        console.log(error);
        yield put({
            type: PASSWORD_ERRORS,
            payload: error.response.data.errors
        });
    }
}
