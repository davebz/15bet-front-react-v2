import { all } from 'redux-saga/effects';
import { transactionHistoryWatcher } from './transaction';
import { bannerWatcher } from './banner';
import { playerWatcher } from './player';
import { paymentWatcher } from './payment';
import { authenticationWatcher } from './authentication';
import { registrationWatcher } from './registration';

export default function* rootSaga() {
    yield all([
        transactionHistoryWatcher(),
        bannerWatcher(),
        authenticationWatcher(),
        playerWatcher(),
        paymentWatcher(),
        registrationWatcher()
    ]);
}
