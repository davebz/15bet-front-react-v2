import { takeLatest, put, call } from "redux-saga/effects";
import { postUser } from "../api/authentication";
import setAuthToken from "../setAuthToken";

import {
    SET_CURRENT_USER,
    GET_ERRORS,
    GET_USER,
    DELETE_USER
} from "../actions/types";

export { authenticationWatcher };

function* authenticationWatcher() {
    yield takeLatest(GET_USER, fetchUser)
    yield takeLatest(DELETE_USER, removeUser)
}

function* fetchUser(action) {
    try {
        const result = yield call(postUser, action.payload.user);
        const { token } = result.data;
        const playerName = result.data.player.Username;

        localStorage.setItem("appToken", token);
        localStorage.setItem("playerName", playerName);

        setAuthToken(token);
        yield put({
            type: SET_CURRENT_USER,
            payload: result.data.token
        });
        action.payload.history.push('/myaccount/profile');
    } catch (error) {
        yield put({
            type: GET_ERRORS,
            payload: error.response.data
        });
    }
}

function* removeUser(action) {
    const history = action.payload;
    yield put({
        type: SET_CURRENT_USER,
        payload: {}
    });
    localStorage.removeItem("appToken");
    localStorage.removeItem("playerName");
    setAuthToken(false);
    history.push("/");
}
