import { takeLatest, put, call } from 'redux-saga/effects';
import {
    getCountries,
    getCurrencies,
    postRegister,
    postVerify
} from "../api/registration";

import setAuthToken from "../setAuthToken";

import {
    GET_BRAND_CURRENCY,
    GET_COUNTRIES,
    REQUEST_POST_VERIFY,
    REQUEST_POST_REGISTER,
    GET_REGISTER_ERRORS,
    GET_REGISTER2_ERRORS,
    REGISTER,
    REGISTER_SET_PAGE,
    SET_BRAND_CURRENCY,
    SET_COUNTRIES,
    SET_CURRENT_USER
} from "../actions/types";

export {
    registrationWatcher
};

function* registrationWatcher() {
    yield takeLatest(GET_BRAND_CURRENCY, fetchBrandCurrency)
    yield takeLatest(GET_COUNTRIES, fetchCountries)
    yield takeLatest(REQUEST_POST_VERIFY, fetchPostVerify)
    yield takeLatest(REQUEST_POST_REGISTER, fetchPostRegister)
}

function* fetchBrandCurrency() {
    try {
        const result = yield call(getCurrencies);
        yield put({
            type: SET_BRAND_CURRENCY,
            payload: result.data.currencies
        });
    } catch (error) {}
}

function* fetchCountries() {
    try {
        const result = yield call(getCountries);
        yield put({
            type: SET_COUNTRIES,
            payload: result.data
        });
    } catch (error) {}
}

function* fetchPostRegister(action) {
    try {
        const result = yield call(postRegister, action.payload);
        localStorage.setItem("players", JSON.stringify(action.payload));
        localStorage.setItem("code", result.data.code);
        yield put({
            type: REGISTER,
            payload: result.data
        });
        yield put({
            type: REGISTER_SET_PAGE,
            payload: 2
        });
    } catch (error) {
        yield put({
            type: GET_REGISTER_ERRORS,
            payload: error.response.data.errors
        });
    }
}

function* fetchPostVerify(action) {
    try {
        const result = yield call(postVerify, action.payload);
        const { token } = result.data.data;
        const playerName = action.payload.username;

        localStorage.setItem("appToken", token);
        localStorage.setItem("playerName", playerName);

        setAuthToken(token);
        yield put({
            type: REGISTER_SET_PAGE,
            payload: 3
        });
        yield put({
            type: SET_CURRENT_USER,
            payload: token
        });
    } catch (error) {
        yield put({
            type: GET_REGISTER2_ERRORS,
            payload: error.response.data.errors
        });
    }
}
