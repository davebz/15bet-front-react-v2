import { takeLatest, takeLeading, put, call } from 'redux-saga/effects';
import { checkDeposit, checkWithdrawal, postDeposit, postWithdrawal, getBankAccount } from "../api/payment"

import {
    DEPOSIT_CHECK,
    DEPOSIT_ERRORS,
    DEPOSIT_SUCCESS,
    WITHDRAWAL_CHECK,
    WITHDRAWAL_ERRORS,
    SET_BANK_ACCOUNT,
    REQUEST_DEPOSIT_CHECK,
    REQUEST_POST_DEPOSIT,
    REQUEST_POST_WITHDRAWAL,
    GET_BANK_ACCOUNT
} from "../actions/types";

export { paymentWatcher };

function* paymentWatcher() {
    yield takeLatest(REQUEST_DEPOSIT_CHECK, fetchCheckDeposit)
    yield takeLeading(REQUEST_POST_DEPOSIT, fetchPostDeposit)
    yield takeLatest(GET_BANK_ACCOUNT, fetchBankAccount)
    yield takeLeading(REQUEST_POST_WITHDRAWAL, fetchPostWithdrawal)
}

function* fetchCheckDeposit(){
    try{
        const result = yield call(checkDeposit);
        yield put({
            type: DEPOSIT_CHECK,
            payload: result.data
        });
        yield put({
            type: DEPOSIT_SUCCESS,
            payload: result.data
        });
    }catch(error){
        yield put({
            type: DEPOSIT_ERRORS,
            payload: error.response.data.errors
        });
    }
}

function* fetchCheckWithdrawal(){
    try{
        const result = yield call(checkWithdrawal);
        yield put({
            type: WITHDRAWAL_CHECK,
            payload: result.data
        });
    }catch(error){
        yield put({
            type: WITHDRAWAL_ERRORS,
            payload: error.response.data.errors
        });
    }
}

function* fetchPostDeposit(action){
    try {
        const result = yield call(postDeposit, action.payload);
        yield put({
            type: DEPOSIT_SUCCESS,
            payload: result.data
        });
        yield* fetchCheckDeposit()
    }catch(error){
        yield put({
            type: DEPOSIT_ERRORS,
            payload: error.response.data.errors
        });
    }
}

function* fetchPostWithdrawal(action){
    try {
        const result = yield call(postWithdrawal, action.payload);
        yield put({
            type: WITHDRAWAL_CHECK,
            payload: result.data
        });
        yield* fetchCheckWithdrawal()
    }catch(error){
        yield put({
            type: WITHDRAWAL_ERRORS,
            payload: error.response.data.errors
        });
    }
}

function* fetchBankAccount(action){
    try {
        const result = yield call(getBankAccount, action.payload);
        yield put({
            type: SET_BANK_ACCOUNT,
            payload: result.data
        });
        yield* fetchCheckWithdrawal()
    }catch(error){
        yield put({
            type: DEPOSIT_ERRORS,
            payload: error.response.data.errors
        });
    }
}
