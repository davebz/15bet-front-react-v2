export const ROOT_URL = process.env.REACT_APP_LOCAL;
export const BO_URL = process.env.REACT_APP_BO;


export function findToken() {
  return localStorage.getItem("appToken");
}
