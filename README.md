This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Prerequisites
1. docker
2. docker-compose

## Get Started

In the project directory, you can run:
### `docker-compose up`

then can navigate to http://localhost:3000, if you want to change port can change port under docker-compose.yml and restart docker-compose


