# base image
FROM node:10.15.3

# set working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /usr/src/app/package.json
RUN npm install yarn@1.15.2 -g
RUN yarn install

# start app
CMD ["yarn", "start"]
